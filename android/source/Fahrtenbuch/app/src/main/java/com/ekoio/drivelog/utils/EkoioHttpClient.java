/*
 * EkoioHttpClient
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

public class EkoioHttpClient {

    /** Vordefinierte URLs **/
    public static final String SUPPORT_URL = "https://www.ekoio.com";
    public static final String LOGIN_URL = "https://stage.ekoio.com/de/rest-auth/login/";
    public static final String LOGOUT_URL = "https://stage.ekoio.com/de/rest-auth/logout/";
    public static final String SMALL_TRIPS_URL = "https://stage.ekoio.com/api/v1/drivedata/small-trips/";
    public static final String TRIP_INFOS_URL = "https://stage.ekoio.com/api/v1/drive_log/drive_logs/extract_drive_log_data_from_trip/?trip_pk=";
    public static final String DRIVE_LOGS_URL = "https://stage.ekoio.com/api/v1/drive_log/drive_logs/";

    /** Vordefinierte Request-Arten **/
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String PUT = "PUT";

    /** Fehlermeldung wegen fehlender Interverbindung **/
    public static final String NETWORK_ERROR = "UnknownHostException";

    private String url, result, requestMethod, sessionCookie;
    private boolean requestSuccessfull;

    /**
     * Initialisiert URL für neue Verbindung. Anschließend 'make*Request(*);' ausführend!
     * @param url URL (bspw. EkoioHttpClient.LOGIN_URL)
     */
    public EkoioHttpClient(String url) {
        this.url = url;
        Logger.log(this.getClass(), "EkoioHttpClient: URL = " + this.url);
    }

    /**
     * Ausführen eines Requests mit Übergabe der Request-Art (Klassenvariable von EkoioHttpClient).
     * Für POST-Request, 2. Parameter = Request-Body als JSON.
     * Für GET-Request, 2. Parameter = Session-Cookie
     * @param requestMethod POST, GET, ... (bspw. EkoioHttpClient.POST)
     * @param param Request-Body (POST) oder Session-Cookie (GET)
     */
    public void makeRequest(String requestMethod, String param){
        if(requestMethod.equals(this.POST)) {
            String requestBody = param;
            makeRequest(requestMethod, "", requestBody);
        } else if (requestMethod.equals(this.GET)) {
            String sessionCookie = param;
            makeRequest(requestMethod, sessionCookie, null);
        }

    }

    /**
     * Ausführen eines Requests mit Übergabe der Request-Art (Klassenvariable von EkoioHttpClient),
     * Session-Cookies und Request-Bodys (für POST, sonst null)
     * @param requestMethod POST, GET, ... (bspw. EkoioHttpClient.POST)
     * @param sessionCookie Session-Cookie (nicht null, dann makeRequest(String) nutzen!)
     * @param requestBody Request-Body als JSON für POST, sonst null!
     */
    public void makeRequest(String requestMethod, String sessionCookie, String requestBody) {

        // Initialisierung
        this.result = "";
        this.requestSuccessfull = false;
        this.requestMethod = requestMethod;
        this.sessionCookie = sessionCookie;

        try {

            URL website = new URL(this.url);
            HttpURLConnection conn = (HttpURLConnection) website.openConnection();

            // Session-Cookie aus EkoioWebportal zuweisen, falls vorhanden
            if (!this.sessionCookie.equals("")) {
                Logger.log(this.getClass(), "makeRequest: Cookie = " + this.sessionCookie);
                Logger.log(this.getClass(), "makeRequest: Referer = " + this.url);
                Logger.log(this.getClass(), "makeRequest: CSRFToken = " + this.sessionCookie.split(";")[0].split("=")[1]);

                conn.setRequestProperty("Cookie", this.sessionCookie);
                conn.setRequestProperty("Referer", this.url);
                conn.setRequestProperty("X-CSRFToken", this.sessionCookie.split(";")[0].split("=")[1]);
            }

            // Vorbereitung der Verbindung entsprechend der Request-Art
            Logger.log(this.getClass(), "makeRequest: Method = " + this.requestMethod);
            if (this.requestMethod.equals(this.POST) || this.requestMethod.equals(this.PUT)) {
                conn.setDoOutput(true);
                conn.setRequestMethod(this.requestMethod);
                conn.setRequestProperty("Content-Type", "application/json");

                // Request-Body schreiben und absenden
                Logger.log(this.getClass(), "makeRequest: requestBody = " + requestBody);
                OutputStream os = conn.getOutputStream();
                os.write(requestBody.getBytes());
                os.flush();
            } else if (this.requestMethod.equals(this.GET)) {
                conn.setRequestMethod("GET");
            } else {
                // Standard, falls unbekannte Request-Methode angegeben ist
                conn.setRequestMethod("GET");
            }

            // Auswertung des Requests
            InputStream in;
            int responseCode = conn.getResponseCode();
            switch (responseCode) {
                case HttpURLConnection.HTTP_OK:
                    in = conn.getInputStream();
                    requestSuccessfull = true;
                    break;
                case HttpURLConnection.HTTP_CREATED:
                    in = conn.getInputStream();
                    requestSuccessfull = true;
                    break;
                default:
                    in = conn.getErrorStream();
                    requestSuccessfull = false;
                    Logger.log(this.getClass(), "makeRequest: getResponseCode = " + responseCode);
                    break;
            }

            // Session-Cookie auslesen und speichern (bleibt gleich, falls bereits übergeben)
            try {
                Map<String, List<String>> headerFields = conn.getHeaderFields();
                List<String> sessionHeaderFields = headerFields.get("Set-Cookie");
                for (String sessionHeader : sessionHeaderFields) {
                    this.sessionCookie += sessionHeader.split(";")[0] + "; ";
                }
            } catch (Exception e) {
            }

            // Request-RÜckgabe auslesen und speichern
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while (true) {
                line = br.readLine();
                if (line != null) {
                    if (this.result == "") this.result = line;
                    else this.result += "\n" + line;
                } else break;
            }
            br.close();
            in.close();

            // Verbindung schließen
            conn.disconnect();
        } catch (UnknownHostException e) {
            this.result = NETWORK_ERROR;
        } catch (Exception e) {
            this.result = e.getMessage();
            e.printStackTrace();
        }
    }

    /**
     * Abfrage, ob letzter ausgeführter Request erfolgreich war.
     * @return True, wenn erfolgreich
     */
    public Boolean requestSuccessfull() {
        return this.requestSuccessfull;
    }

    /**
     * Rückgabe der Request-Antwort als String
     * @return Request-Response (i.d.R. JSON-String)
     */
    public String getResult() {
        Logger.log(this.getClass(), "getResult: " + this.result);
        return this.result;
    }

    /**
     * Rückgabe des Session-Cookies der letzten Request-Antwort
     * @return Session-Cookie der Form: csrftoken=XXXXX; sessionid=XXXXX;
     */
    public String getSessionCookie() {
        Logger.log(this.getClass(), "getSessionCookie: " + this.sessionCookie);
        return this.sessionCookie;
    }


    /*


     * Ausführen eines POST-Request mit Übergabe des JSONs und Anlegen eines neuen Verbindungstoken
     * @param jsonPOST JSON-String der gesendet werden soll

    public void makePOSTRequest(String jsonPOST) {
        makePOSTRequest(jsonPOST, "");
    }


     * Ausführen eines POST-Request mit Übergabe des JSONs und Verbindungstoken
     * @param jsonPOST JSON-String der gesendet werden soll

    public void makePOSTRequest(String jsonPOST, String sessionCookie) {
        this.result = null;
        this.requestSuccessfull = false;
        this.sessionCookie = sessionCookie;

        try {
            URL website = new URL(this.url);
            HttpURLConnection conn = (HttpURLConnection) website.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            // Request-Body schreiben und absenden
            Logger.log(this.getClass(), "jsonPOST: " + jsonPOST);
            OutputStream os = conn.getOutputStream();
            os.write(jsonPOST.getBytes());
            os.flush();

            // Auswertung des Requests
            InputStream in;
            if (conn.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST){
                in = conn.getErrorStream();
                requestSuccessfull = false;
            } else if (conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                in = conn.getInputStream();
                requestSuccessfull = true;
            } else{
                throw new RuntimeException(" " + conn.getResponseCode());
            }

            // Session-Cookie auslesen und speichern
            try {
                Map<String, List<String>> headerFields = conn.getHeaderFields();
                List<String> sessionHeaderFields = headerFields.get("Set-Cookie");
                for (String sessionHeader : sessionHeaderFields) {
                    this.sessionCookie += sessionHeader.split(";")[0] + "; ";
                }
            } catch (Exception e) {}

            // Request-RÜckgabe auslesen und speichern
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while (true) {
                line = br.readLine();
                if (line != null) {
                    if (this.result == null) this.result = line;
                    else this.result += "\n" + line;
                } else break;
            }
            br.close();

            conn.disconnect();
        } catch (Exception e) {
            Log.e("EkoioHttpClient", e.getMessage());
        }
    }


     * Ausführen eines GET-Request unter Angabe des Session-Cookies.
     * @param sessionCookie Session-Cookie

    public void makeGETRequest(String sessionCookie) {
        this.result = null;
        this.requestSuccessfull = false;
        this.sessionCookie = sessionCookie;

        try {

            URL website = new URL(this.url);
            HttpURLConnection conn = (HttpURLConnection) website.openConnection();
            conn.setRequestMethod("GET");

            // Session-Cookie aus EkoioWebportal zuweisen
            conn.setRequestProperty("Cookie", this.sessionCookie);

            // Auswertung des Requests
            InputStream in;
            if (conn.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST){
                in = conn.getErrorStream();
                requestSuccessfull = false;
            } else if (conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                in = conn.getInputStream();
                requestSuccessfull = true;
            } else{
                throw new RuntimeException(" " + conn.getResponseCode());
            }

            // Request-RÜckgabe auslesen und speichern
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while (true) {
                line = br.readLine();
                if (line != null) {
                    if (this.result == null) this.result = line;
                    else this.result += "\n" + line;
                } else break;
            }
            br.close();

            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    */
}
