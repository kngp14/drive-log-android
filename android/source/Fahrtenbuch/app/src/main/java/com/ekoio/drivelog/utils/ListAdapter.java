/*
 * ListAdapter
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ekoio.drivelog.R;
import com.ekoio.drivelog.ui.EditorActivity;

import java.util.ArrayList;



public class ListAdapter extends ArrayAdapter<ListEntries> {

    Context context;

    public ListAdapter(Context context, ArrayList<ListEntries> listEntries) {
        super(context, 0, listEntries);
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Item für die Position bekommen
        final ListEntries listEntry = getItem(position);
        // Check ob existierender View erneut benutzt wird, ansonsten erweitern
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.component_list_trips, parent, false);
        }
        // Lookup view for data population
        final ImageView ivStatus = (ImageView) convertView.findViewById(R.id.comp_list_trips_imageview_status);
        ImageView ivArrow = (ImageView) convertView.findViewById(R.id.comp_list_trips_imageview_arrow);
        TextView tvStart = (TextView) convertView.findViewById(R.id.comp_list_trips_textview_start_address);
        TextView tvEnd = (TextView) convertView.findViewById(R.id.comp_list_trips_textview_end_address);
        TextView tvDistance = (TextView) convertView.findViewById(R.id.comp_list_trips_textview_distance);
        TextView tvDate = (TextView) convertView.findViewById(R.id.comp_list_trips_textview_date);
        TextView tvCustomize = (TextView) convertView.findViewById(R.id.comp_list_trips_textview_customize);

        // Angelegte Fahrten (ohne zugeordnete Fahrt)
        if(listEntry.date.startsWith("+")) {

            tvDate.setText(listEntry.date);
            tvDate.setTypeface(null, Typeface.NORMAL);
            tvDate.setTextColor(context.getResources().getColor(R.color.colorPrimary));

            ivArrow.setVisibility(View.GONE);
            ivStatus.setVisibility(View.GONE);
            tvStart.setVisibility(View.GONE);
            tvEnd.setVisibility(View.GONE);
            tvDistance.setVisibility(View.GONE);
            tvCustomize.setVisibility(View.GONE);


        } else {
            // bei Klick auf "BEARBEITEN" oder "Ansicht" neue Activity starten und benötigte Daten übergeben
            tvCustomize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent editorActivity = new Intent(v.getContext(), EditorActivity.class);
                    editorActivity.putExtra("trip", listEntry.id);
                    ((Activity) context).startActivityForResult(editorActivity, EditorActivity.EDITOR_REQUEST_CODE);
                }
            });


            tvDate.setText(listEntry.date);
            tvDate.setTypeface(null, Typeface.BOLD);
            tvDate.setTextColor(context.getResources().getColor(R.color.colorBlack));
            ivArrow.setVisibility(View.VISIBLE);
            ivStatus.setVisibility(View.VISIBLE);
            tvStart.setVisibility(View.VISIBLE);
            tvEnd.setVisibility(View.VISIBLE);
            tvDistance.setVisibility(View.VISIBLE);
            tvCustomize.setVisibility(View.VISIBLE);

            // Daten in Textview schreiben
            tvStart.setText(listEntry.start + ", " + listEntry.startTime);
            tvEnd.setText(listEntry.end + ", " + listEntry.endTime);
            tvDistance.setText(listEntry.distance + "\nkm");

            // Prüfung des Status --> anzuzeigendes Bild bestimmen
            if (listEntry.status.equals("categorized")) {
                ivStatus.setImageResource(R.drawable.ic_processed_trip);
            } else {
                ivStatus.setImageResource(R.drawable.ic_unprocessed_trip);
            }
        }
        return convertView;
    }
}
