/*
 * LoginActivity
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.ui;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.CancellationSignal;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ekoio.drivelog.R;
import com.ekoio.drivelog.utils.Account;
import com.ekoio.drivelog.utils.CredentialStorageManager;
import com.ekoio.drivelog.utils.EkoioHttpClient;
import com.ekoio.drivelog.utils.EkoioWebportal;
import com.ekoio.drivelog.utils.Logger;
import com.ekoio.drivelog.utils.PermissionRequestHelper;

public class LoginActivity extends AppCompatActivity implements PermissionRequestHelper.OnPermissionResult {

    // Anmeldevorgang zwischenspeichern, um ggf. bei Fehlern abberechen zu können
    private LoginTask mAuthTask = null;

    // Fingerabdruck Permission-Request
    private final static int REQUEST_SET_FINGERPRINT_IN_ANDROID_SETTINGS = 110;
    private boolean readyForFingerprint = false;
    private FingerprintManager fingerprintManager;
    private LinearLayout fingerPrintLayout;

    // UI references.
    private Button buttonLogin;
    private Button buttonHelp;
    private TextInputEditText editTextUserEmail;
    private TextInputLayout layoutEmail;
    private TextInputEditText editTextPassword;
    private TextInputLayout layoutPassword;
    private CheckBox checkboxSaveCredentials;
    private ProgressBar progressBar;

    // Berechtigungsmanager
    private PermissionRequestHelper permissionRequestHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /** UI-Elemente einlesen **/
        progressBar = (ProgressBar) findViewById(R.id.ac_login_button_progress);
        checkboxSaveCredentials = (CheckBox) findViewById(R.id.ac_login_checkBox_savecredentials);
        fingerPrintLayout = (LinearLayout) findViewById(R.id.ac_login_linearlayout_fingerprint);

        /** Initialisierung der GUI **/
        resetErrors();
        configureLoginButton();
        configureHelpButton();

        /** Zugangsdaten laden, falls bereits gespeichert **/
        loadCredentials();

        /** Berechtigung für Internetverbindung **/
        permissionRequestHelper = new PermissionRequestHelper();
        permissionRequestHelper.requestPermission(this, Manifest.permission.INTERNET);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SET_FINGERPRINT_IN_ANDROID_SETTINGS:
                finish();
                startActivity(new Intent(this, this.getClass()));
				break;
            default:
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionRequestHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onPermissionResult(String permission, boolean isGranted) {
        switch (permission) {
            case Manifest.permission.INTERNET:
                if (isGranted) {
                    Logger.log(this.getClass(), "Berechtigung INTERNET erhalten");

                    // Weitere Berechtigung für ACCESS_NETWORK_STATE anfordern
                    permissionRequestHelper.requestPermission(LoginActivity.this, Manifest.permission.ACCESS_NETWORK_STATE);
                } else {
                    Logger.log(this.getClass(), "Berechtigung INTERNET verweigert");

                    // TODO: Fehlermeldung und erneute Berechtigungsanforderung
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                    builder.setTitle("Berechtigung INTERNET verweigert");
                    builder.setMessage("Fehler");
                    builder.show();
                }
                break;

            case Manifest.permission.ACCESS_NETWORK_STATE:
                if (isGranted) {
                    Logger.log(this.getClass(), "Berechtigung ACCESS_NETWORK_STATE erhalten");

                    /** Fingerabdrucksensor ab Android Marshmallow möglich **/
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        /** Berechtigung zum Lesen des Fingerabdrucks prüfen **/
                        permissionRequestHelper.requestPermission(LoginActivity.this, Manifest.permission.USE_FINGERPRINT);

                    } else {
                        Logger.log(this.getClass(), "Android Version unterstützt kein Fingerabdruck-Sensor");
                    }
                } else {
                    Logger.log(this.getClass(), "Berechtigung ACCESS_NETWORK_STATE verweigert");

                    // TODO: Fehlermeldung und erneute Berechtigungsanforderung
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                    builder.setTitle("Berechtigung ACCESS_NETWORK_STATE verweigert");
                    builder.setMessage("Fehler");
                    builder.show();
                }

                break;

            case Manifest.permission.USE_FINGERPRINT:
                if (isGranted) {
                    Logger.log(this.getClass(), "Berechtigung USE_FINGERPRINT erhalten");

                    /** Prüfen, ob Gerät Fingerabdrucksensor hat **/
                    fingerprintManager = (FingerprintManager) getSystemService(Context.FINGERPRINT_SERVICE);
                    if (fingerprintManager.isHardwareDetected()) {
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            Logger.log(this.getClass(), "Keine gespeicherten Fingerabdrücke");

                            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                            builder.setTitle(getString(R.string.dialog_title_secure_with_fingerprint));
                            builder.setMessage(R.string.dialog_content_create_fingerprint_in_settings);
                            builder.setPositiveButton(R.string.dialog_positive_text_open_settings, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    startActivityForResult(new Intent(Settings.ACTION_SECURITY_SETTINGS), REQUEST_SET_FINGERPRINT_IN_ANDROID_SETTINGS);
                                }
                            } );
                            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            } );
                            builder.show();

                        } else {
                            this.readyForFingerprint = true;
                            CredentialStorageManager csm = new CredentialStorageManager(this);
                            if(!csm.getFingerprintProtection().equals("false")) {
                                Logger.log(this.getClass(), "Fingerabdruck muss/kann zur Identifizierung geprüft werden");
                                fingerPrintLayout.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        Logger.log(this.getClass(), "Kein Fingerabdrucksensoor verbaut");
                    }
                } else {
                    Logger.log(this.getClass(), "Berechtigung USE_FINGERPRINT verweigert");

                    // TODO: Fehlermeldung mit Hinweis, dass dann keine Fingerabdrucker-Absicherung erfolgen kann
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                    builder.setTitle("Berechtigung USE_FINGERPRINT verweigert");
                    builder.setMessage("Fehler");
                    builder.show();
                }
                break;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void activateFingerprintScanner(final MaterialDialog dialog) {
        FingerprintManager.AuthenticationCallback callback = new FingerprintManager.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errMsgId, CharSequence errString) {
                Toast.makeText(getApplicationContext(), getString(R.string.msg_wrong_fingerprint) +  "\n" + errString, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                mAuthTask =null;
            }

            @Override
            public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
                Toast.makeText(getApplicationContext(), helpString, Toast.LENGTH_SHORT).show();

                new AsyncTask<Void, Void, Void>() {

                    ImageView imageView = (ImageView) dialog.getCustomView().findViewById(R.id.comp_dialog_finger_image);

                    protected void onPreExecute() {
                        imageView.setColorFilter(getResources().getColor(R.color.colorOrange), PorterDuff.Mode.SRC_IN);

                    }
                    protected Void doInBackground(Void... unused) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    protected void onPostExecute(Void unused) {
                        imageView.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    }
                }.execute();
            }

            @Override
            public void onAuthenticationFailed() {
                Toast.makeText(getApplicationContext(), R.string.msg_wrong_fingerprint, Toast.LENGTH_SHORT).show();

                new AsyncTask<Void, Void, Void>() {

                    ImageView imageView = (ImageView) dialog.getCustomView().findViewById(R.id.comp_dialog_finger_image);

                    protected void onPreExecute() {
                        imageView.setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_IN);

                    }
                    protected Void doInBackground(Void... unused) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    protected void onPostExecute(Void unused) {
                        imageView.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    }
                }.execute();
            }

            @Override
            public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

                new AsyncTask<Void, Void, Void>() {
                    protected void onPreExecute() {
                        ImageView imageView = (ImageView) dialog.getCustomView().findViewById(R.id.comp_dialog_finger_image);
                        imageView.setColorFilter(getResources().getColor(R.color.colorGreen), PorterDuff.Mode.SRC_IN);

                    }
                    protected Void doInBackground(Void... unused) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                    protected void onPostExecute(Void unused) {

                        dialog.dismiss();

                        // Ladeanimation starten und Anmeldung durchführen
                        showProgress(true);
                        mAuthTask.execute((Void) null);
                    }
                }.execute();


            }
        };
        fingerprintManager.authenticate(null, new CancellationSignal(), 7, callback, null);
    }

    /**
     * Fehlermeldungen zurücksetzen
     */
    private void resetErrors() {
        layoutEmail = (TextInputLayout) findViewById(R.id.ac_login_inputlayout_user);
        editTextUserEmail = (TextInputEditText) findViewById(R.id.ac_login_input_user);
        editTextUserEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                layoutEmail.setError(null);
                return false;
            }
        });
        editTextUserEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && readyForFingerprint)
                    fingerPrintLayout.setVisibility(View.GONE);
                else if(readyForFingerprint)
                    fingerPrintLayout.setVisibility(View.VISIBLE);
            }
        });
        layoutPassword = (TextInputLayout) findViewById(R.id.ac_login_inputlayout_password);
        editTextPassword = (TextInputEditText) findViewById(R.id.ac_login_input_password);
        editTextPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                layoutPassword.setError(null);
                return false;
            }
        });
        editTextPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && readyForFingerprint)
                    fingerPrintLayout.setVisibility(View.GONE);
                else if(readyForFingerprint)
                    fingerPrintLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Funktionszuweisung des Anmelde-Button
     */
    private void configureLoginButton() {
        this.buttonLogin = (Button) findViewById(R.id.act_login_button_login);
        this.buttonLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        setPreLollipopElevation(this.buttonLogin);
    }

    /**
     * Funktionszuweisung des Hilfe-Button
     */
    private void configureHelpButton() {
        this.buttonHelp = (Button) findViewById(R.id.act_login_button_help);
        this.buttonHelp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext(), R.style.AlertDialogStyle);
                builder.setTitle(R.string.dialog_title_more_infos);
                builder.setMessage(R.string.dialog_content_more_infos
                );
                builder.setPositiveButton(R.string.dialog_positive_text_more_infos,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.setNeutralButton(R.string.dialog_neutral_text_more_infos,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(EkoioHttpClient.SUPPORT_URL));
                                startActivity(browserIntent);
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        setPreLollipopElevation(this.buttonHelp);
    }

    /**
     * Abwärtskompatibilität für Schatten
     * @param view Element mit Elevation
     */
    private void setPreLollipopElevation(View view) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) view.setBackground(getResources().getDrawable(android.R.drawable.dialog_holo_light_frame));
    }

    /**
     * Benutzerdaten laden, falls vorhanden
     */
    private void loadCredentials() {
        SharedPreferences sp = getSharedPreferences("DriveLog", MODE_PRIVATE);
        if(sp.getBoolean(CredentialStorageManager.SETTINGS_SAVEDCREDENTIALS, false)) {
            CredentialStorageManager csm = new CredentialStorageManager(this);
            Account account = csm.getCredentials();
            if(!account.getPassword().equals("ERROR") && !account.getPassword().equals("EMPTY")) {
                if(account.getEmail().equals("")) {
                    editTextUserEmail.setText(account.getUsername());
                } else {
                    editTextUserEmail.setText(account.getEmail());
                }
                editTextPassword.setText(account.getPassword());
                checkboxSaveCredentials.setChecked(true);
            } else if(account.getPassword().equals("ERROR")) {
                csm.deleteCredentials();
            }
        }
    }

    /**
     * Prüfung der Eingabefelder auf formale Eingabefehler und starten des Anmeldevorgangs
     */
    private void attemptLogin() {
        if (mAuthTask == null) {

            // Fehlermeldungen zurücksetzen
            layoutEmail.setError(null);
            layoutPassword.setError(null);
            boolean cancelLogin = false;
            View focusedViewWithError = null;

            // Anmeldedaten einlesen
            String user = editTextUserEmail.getText().toString();
            String password = editTextPassword.getText().toString();


            // Fehlermeldung, falls Passwortfeld leer
            if (TextUtils.isEmpty(password)) {
                layoutPassword.setError(getString(R.string.msg_error_empty_field));
                editTextPassword.setText("");
                focusedViewWithError = editTextPassword;
                cancelLogin = true;
            }

            // Fehlermeldung, falls E-Mail-Feld leer oder ungültiges E-Mail-Format
            if (TextUtils.isEmpty(user)) {
                layoutEmail.setError(getString(R.string.msg_error_empty_field));
                focusedViewWithError = editTextUserEmail;
                cancelLogin = true;
            }

            // Anmeldung durchführen, sofern keine formalen Eingabefehler
            if (cancelLogin) {
                // Eingabefehler --> Eingabefeld mit Fehler fokussieren
                focusedViewWithError.requestFocus();
            } else {
                mAuthTask = new LoginTask(this, user, password, checkboxSaveCredentials.isChecked());

                if(readyForFingerprint) {
                    final CredentialStorageManager csm = new CredentialStorageManager(this);
                    if(csm.getFingerprintProtection().equals("Unknown")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                        builder.setTitle(R.string.dialog_title_secure_with_fingerprint);
                        builder.setMessage(R.string.dialog_content_secure_with_fingerprint);
                        builder.setPositiveButton(R.string.dialog_positive_text_secure_with_fingerprint, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(csm.saveFingerprintProtection(true)) {
                                    dialog.dismiss();
                                    showFingerPrintDialog(true);
                                } else {
                                    Logger.log(this.getClass(), "Fehler beim Speichern der Antwort");
                                }
                            }
                        });
                        builder.setNegativeButton(R.string.dialog_negative_text_secure_with_fingerprint,  new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                csm.saveFingerprintProtection(false);
                                dialog.dismiss();
                                mAuthTask.execute((Void) null);
                            }
                        });
                        builder.show();
                    } else if(csm.getFingerprintProtection().equals("true")) {
                        showFingerPrintDialog(false);
                    }else if(csm.getFingerprintProtection().equals("false")) {
					
                        // Ladeanimation starten und Anmeldung durchführen
                        showProgress(true);
                        mAuthTask.execute((Void) null);
						
                    }
                } else {
                    showProgress(true);
                    mAuthTask.execute((Void) null);
                }
            }
        }
    }

    private void showFingerPrintDialog(boolean firstTime) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .customView(R.layout.component_dialog_fingerprint, true)
                .cancelable(false);

        MaterialDialog dialog = builder.build();

        TextView txtCotent = (TextView) dialog.getCustomView().findViewById(R.id.comp_dialog_finger_content);
        if(firstTime) {
            txtCotent.setText(R.string.dialog_content_create_fingerprint);
        } else {
            txtCotent.setText(R.string.dialog_content_scan_fingerprint);
        }

        dialog.show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activateFingerprintScanner(dialog);
        }
    }

    /**
     * Ladeanimation ein/ausblenden und Anmeldemaske ausblenden
     * @param show true zum Einblenden
     */
    private void showProgress(final boolean show) {
        String buttonLoginText;
        int colorText;
        int progressVisibility;
        if(show) {
            buttonLoginText = "";
            colorText = getResources().getColor(R.color.colorDarkGray);
            progressVisibility = View.VISIBLE;
        } else {
            buttonLoginText = getString(R.string.activity_login_button_login);
            colorText = getResources().getColor(R.color.colorWhite);
            progressVisibility = View.GONE;
        }
        this.buttonLogin.setTextColor(colorText);
        this.buttonLogin.setText(buttonLoginText);
        this.progressBar.setVisibility(progressVisibility);
        this.progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorDarkGray), android.graphics.PorterDuff.Mode.MULTIPLY);
        this.buttonLogin.setEnabled(!show);
    }


    /**
     * Anmeldevorgang als asynchrone Aufgabe
     */
    private class LoginTask extends AsyncTask<Void, Void, String> {

        private final Account account;
        private final boolean saveCredentials;
        private final Context context;

        /**
         * Anmeldungevorgang mit übergebenen EkoioWebportal-Daten anlegen.
         * Ausführen der Anmeldung mit execute()-Methode
         * @param user E-Mail Adresse oder Benutzername des ekoio-Accounts
         * @param password Passwort
         * @param saveCredentials True - Zugangsdaten werden gespeichert
         */
        LoginTask(Context context, String user, String password, boolean saveCredentials) {
            this.context = context;

            this.account = new Account();
            if(user.contains("@"))
                this.account.setEmail(user);
            else
                this.account.setUsername(user);
            this.account.setPassword(password);
            this.saveCredentials = saveCredentials;
        }

        @Override
        protected String doInBackground(Void... params) {
            // Login ausführen
            EkoioWebportal ekoioWebportal = new EkoioWebportal();
            String returnMessage = ekoioWebportal.login(
                    this.context,
                    this.account,
                    this.saveCredentials);
            return returnMessage;
        }

        @Override
        protected void onPostExecute(final String returnMessage) {
            mAuthTask = null;

            if (returnMessage.equals(EkoioWebportal.LOGIN_SUCCESSFULL)) {
                Intent mainActivity = new Intent(LoginActivity.this, MainActivity.class);
                mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainActivity);
                finish();
            } else if (returnMessage.toLowerCase().contains("e-mail")) {
                showProgress(false);
                layoutEmail.setError(returnMessage);
                editTextUserEmail.setText("");
                editTextUserEmail.requestFocus();
                editTextPassword.setText("");
            } else if(returnMessage.equals(EkoioWebportal.NETWORK_ERROR)) {
                new MaterialDialog.Builder(this.context)
                        .title(R.string.dialog_title_failed_internet_connection)
                        .content(R.string.dialog_content_failed_internet_connection)
                        .positiveText(R.string.dialog_positive_failed_internet_connection)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                                startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 999);

                            }
                        })
                        .negativeText(R.string.dialog_negative_text_failed_internet_connection)
                        .show();
                showProgress(false);
                layoutPassword.setError(this.context.getString(R.string.msg_failed_internet_connection));
                editTextPassword.setText("");
                editTextPassword.requestFocus();
            } else{
                showProgress(false);
                layoutPassword.setError(returnMessage);
                editTextPassword.setText("");
                editTextPassword.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }
}

