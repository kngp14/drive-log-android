/*
 * WaveView
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.ekoio.drivelog.R;

public class WaveView extends View {
    private float width;
    private float height;
    private int tintColor;
    private Path path;
    private RectF viewBounds;
    private Paint paint;
    private int wavesCount;
    private int wavesHeight;
    private int colorAlpha;

    private static final int DEFAULT_WAVES_COUNT = 3;
    private static final int DEFAULT_WAVES_HEIGHT_DP = 30;
    private final static int DEFAULT_COLOR_ALPHA = 50;
    private final static int DEFAULT_TINT_COLOR = Color.BLACK;


    public WaveView(Context context) {
        super(context);
        init(context, null);
    }

    public WaveView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public WaveView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        this.wavesCount = DEFAULT_WAVES_COUNT;
        this.wavesHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_WAVES_HEIGHT_DP, context.getResources().getDisplayMetrics());
        this.colorAlpha = DEFAULT_COLOR_ALPHA;
        this.tintColor = DEFAULT_TINT_COLOR;
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.WaveView);

            if (array.hasValue(R.styleable.WaveView_tintColor)) {
                this.tintColor = array.getColor(R.styleable.WaveView_tintColor, DEFAULT_TINT_COLOR);
            }
            if (array.hasValue(R.styleable.WaveView_wavesCount)) {
                this.wavesCount = array.getInt(R.styleable.WaveView_wavesCount, DEFAULT_WAVES_COUNT);
                if (this.wavesCount < 1 || this.wavesCount > 10)
                    this.wavesCount = DEFAULT_WAVES_COUNT;
            }
            if (array.hasValue(R.styleable.WaveView_wavesHeight))
                this.wavesHeight = array.getDimensionPixelSize(R.styleable.WaveView_wavesHeight, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_WAVES_HEIGHT_DP, context.getResources().getDisplayMetrics()));
            if (array.hasValue(R.styleable.WaveView_colorAlpha)) {
                this.colorAlpha = array.getInt(R.styleable.WaveView_colorAlpha, DEFAULT_COLOR_ALPHA);
                if (this.colorAlpha > 255 || this.colorAlpha < 0)
                    this.colorAlpha = DEFAULT_COLOR_ALPHA;
            }
            array.recycle();
        }
        wavesHeight /= 2;
        path = new Path();
        viewBounds = new RectF();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(tintColor);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = getMeasuredWidth();
        height = getMeasuredHeight();
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        int l = wavesCount / 2;
        path = getWavePath(width, height, wavesHeight, l * 20, 4);
        viewBounds.set(0, 0, width, height);
        canvas.clipPath(path);
        canvas.drawColor(Color.WHITE);
        paint.setAlpha(colorAlpha);
        canvas.clipRect(viewBounds, Region.Op.UNION);
        for (int i = 1; i <= wavesCount; i++) {
            path = getWavePath(width, height, wavesHeight, i * i * 20, 3);
            canvas.drawPath(path, paint);
        }
    }


    static Path getWavePath(float width, float height, float amplitude, float shift, float divide) {
        Path path = new Path();
        float quadrant = height - amplitude;
        float x, y;
        path.moveTo(0, 0);
        path.lineTo(0, quadrant);
        for (int i = 0; i < width + 10; i = i + 10) {
            x = (float) i;
            y = quadrant + amplitude * (float) Math.sin(((i + 10) * Math.PI / 180) / divide + shift);
            path.lineTo(x, y);
        }
        path.lineTo(width, 0);
        path.close();
        return path;
    }

    /**
     * Einfärbung anpassen
     * @param color Farbe
     */
    public void setTintColor(@ColorInt int color) {
        this.tintColor = color;
        if (tintColor != 0) {
            if (Color.alpha(tintColor) == 255)
                tintColor = Color.argb(55, Color.red(tintColor), Color.green(tintColor), Color.blue(tintColor));
            paint.setColor(tintColor);
        }
        invalidate();
    }

    /**
     * Anzahl Wellen anpassen
     * @param wavesCount Anzahl zwischen 1 und 10
     */
    public void setWavesCount(int wavesCount) {

        if (wavesCount > 1 && wavesCount < 10)
            this.wavesCount = wavesCount;
        else
            this.wavesCount = DEFAULT_WAVES_COUNT;

        invalidate();
    }
}
