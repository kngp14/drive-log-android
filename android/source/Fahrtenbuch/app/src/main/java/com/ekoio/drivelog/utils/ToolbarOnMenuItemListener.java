/*
 * ToolbarOnMenuItemListener
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.ekoio.drivelog.R;
import com.ekoio.drivelog.ui.LoginActivity;
import com.ekoio.drivelog.ui.MainActivity;
import com.ekoio.drivelog.ui.TripCreatorActivity;
import com.mikepenz.aboutlibraries.Libs;
import com.mikepenz.aboutlibraries.LibsBuilder;


public class ToolbarOnMenuItemListener implements Toolbar.OnMenuItemClickListener {

    private final Context context;

    /**
     * Toolbar-Menü-Listener mit Abmelde-Menuitem
     * @param context Context, auf dem Toolbar vorhanden ist (meist this)
     */
    public ToolbarOnMenuItemListener(Context context) {
        this.context = context;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_action_logout:
                LogoutTask logoutTask = new LogoutTask(this.context);
                logoutTask.execute((Void) null);
                return true;
            case R.id.toolbar_action_refresh:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((SwipeRefreshLayout)((MainActivity) this.context).findViewById(R.id.activity_main)).setRefreshing(true);
                } else {
                    Toast.makeText(this.context, R.string.loading_trips, Toast.LENGTH_SHORT).show();
                }
                ((MainActivity) this.context).update();
                return true;
            case R.id.toolbar_action_info:
                new LibsBuilder()
                        .withActivityTitle(this.context.getString(R.string.info))
                        .withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR)
                        .withAboutIconShown(true)
                        .withAboutAppName(this.context.getString(R.string.app_name))
                        .withVersionShown(true)
                        .withAboutDescription(this.context.getString(R.string.title_libraries_builder))
                        .withLicenseDialog(true)
                        .withLicenseShown(true)
                        .withAboutVersionShown(true)
                        .start(this.context);
                return true;
            default:
                Logger.log(this.getClass(), "onOptionsItemSelected: " + item.getItemId());
                return true;
        }
    }

    /**
     * Abmeldevorgang als asynchrone Aufgabe
     */
    private class LogoutTask extends AsyncTask<Void, Void, String> {

        private final Context context;

        /**
         * Abmeldungevorgang anlegen.
         * Ausführen der Abmeldung mit execute()-Methode
         */
        LogoutTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(Void... params) {
            // Login ausführen
            EkoioWebportal ekoioWebportal = new EkoioWebportal();
            String returnMessage = ekoioWebportal.logout(this.context);
            return returnMessage;
        }

        @Override
        protected void onPostExecute(final String returnMessage) {
            if (returnMessage.equals(EkoioWebportal.LOGOUT_SUCCESSFULL)) {
                Intent mainActivity = new Intent(this.context, LoginActivity.class);
                mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                this.context.startActivity(mainActivity);
                ((Activity) this.context).finish();
            } else{
                Toast.makeText(this.context, context.getString(R.string.msg_failed_logout) + "\n" + returnMessage, Toast.LENGTH_SHORT).show();
            }
        }
    }
}