/*
 * CredentialsManager
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.utils;

import android.content.Context;
import android.content.SharedPreferences;
import static android.content.Context.MODE_PRIVATE;

public class CredentialStorageManager {

    private CryptoManager cm;
    private Context context;
    private Account account;
    private String sessionCookie;

    public final static String SETTINGS_SAVEDCREDENTIALS = "SavedCredentials";
    private final static String SETTINGS_USERNAME = "Username";
    private final static String SETTINGS_EMAIL = "Email";
    private final static String SETTINGS_PASSWORD = "Password";
    private final static String SETTINGS_COOKIE = "Cookie";
    private final static String SETTING_STORE_NAME = "DriveLog";
    private final static String SETTINGS_FINGERPRINT_PROTECTION = "Fingerprint";

    public final static byte[] KEY_BYTES = {-62, -81, 92, 95, 40, -29, -125, -124, 41, 95, 47, -62, -81};

    public CredentialStorageManager(Context co) {
        context = co;
    }

    private void initKeyStore() {
        cm = new CryptoManager();
        cm.createNewKey(context);
    }

    /**
     * Zugangsdaten speichern
     * @param account ekoio-Account
     * @return true, wenn erfolgreich gespeichert
     */
    public boolean saveCredentials(Account account) {
        initKeyStore();
        deleteCredentials();
        this.account = account;
        try {
            String unameCipher = cm.encryptString(this.account.getUsername());
            String emailCipher = cm.encryptString(this.account.getEmail());
            String pwdCipher = cm.encryptString(this.account.getPassword());
            SharedPreferences sp = context.getSharedPreferences(SETTING_STORE_NAME, MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(SETTINGS_USERNAME, unameCipher);
            editor.putString(SETTINGS_EMAIL, emailCipher);
            editor.putString(SETTINGS_PASSWORD, pwdCipher);
            editor.putBoolean(SETTINGS_SAVEDCREDENTIALS, true);
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Auslesen der Zugangsdaten aus Anwendungsspeicher
     * @return Account-Objekt mit Zugangsdaten
     */
    public Account getCredentials() {
        initKeyStore();
        this.account = new Account();
        try {
            SharedPreferences sp = context.getSharedPreferences(SETTING_STORE_NAME, MODE_PRIVATE);
            String unameCipher = sp.getString(SETTINGS_USERNAME, "EMPTY");
            String emailCipher = sp.getString(SETTINGS_EMAIL, "EMPTY");
            String passwordCipher = sp.getString(SETTINGS_PASSWORD, "EMPTY");

            if(!unameCipher.equals("EMPTY") && !emailCipher.equals("EMPTY") && !passwordCipher.equals("EMPTY")) {
                this.account.setUsername(cm.decryptString(unameCipher));
                this.account.setEmail(cm.decryptString(emailCipher));
                this.account.setPassword(cm.decryptString(passwordCipher));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.account;
    }

    /**
     * Löschen der Zugangsdaten und Session Cookies
     */
    public void deleteCredentials() {
        SharedPreferences sp = context.getSharedPreferences(SETTING_STORE_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(SETTINGS_USERNAME);
        editor.remove(SETTINGS_EMAIL);
        editor.remove(SETTINGS_PASSWORD);
        editor.remove(SETTINGS_SAVEDCREDENTIALS);
        editor.remove(SETTINGS_COOKIE);
        editor.apply();
    }

    /**
     * Löschen der Einstellung "Mit Fingerabdruck absichern"
     */
    public void deleteFingerprintProtection() {
        SharedPreferences sp = context.getSharedPreferences(SETTING_STORE_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(SETTINGS_FINGERPRINT_PROTECTION);
        editor.apply();
    }

    /**
     * Session-Cookie speichern
     * @param sessionCookie Cookie
     * @return true, wenn erfolgreich gespeichert
     */
    public boolean saveCookie(String sessionCookie) {
        initKeyStore();
        this.sessionCookie = sessionCookie;
        try {
            String sessionCookieCipher = cm.encryptString(this.sessionCookie);
            SharedPreferences sp = context.getSharedPreferences(SETTING_STORE_NAME, MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(SETTINGS_COOKIE, sessionCookieCipher);
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Rückgabe des aktuellen Session-Cookies
     * @return Cookie als String
     */
    public String getCookie() {
        initKeyStore();
        String sessionCookie = "";
        try {
            SharedPreferences sp = context.getSharedPreferences(SETTING_STORE_NAME, MODE_PRIVATE);
            String sessionCookieCipher = sp.getString(SETTINGS_COOKIE, "EMPTY");
            if(!sessionCookieCipher.equals("EMPTY")) {
                sessionCookie = cm.decryptString(sessionCookieCipher);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sessionCookie;
    }

    /**
     * Speichern, ob Start der Anwendung durch einen Fingerabdruck abgesichert werden soll.
     * @param activated true -> Nach Fingerabdruck fragen
     * @return true -> Erfolgreich gespeichert
     */
    public boolean saveFingerprintProtection(Boolean activated) {
        initKeyStore();
        try {
            SharedPreferences sp = context.getSharedPreferences(SETTING_STORE_NAME, MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(SETTINGS_FINGERPRINT_PROTECTION, activated.toString());
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getFingerprintProtection() {
        initKeyStore();
        String activated = "Unknown";
        try {
            SharedPreferences sp = context.getSharedPreferences(SETTING_STORE_NAME, MODE_PRIVATE);
            String fingerprintProtection = sp.getString(SETTINGS_FINGERPRINT_PROTECTION, "EMPTY");
            if(!fingerprintProtection.equals("EMPTY")) {
                activated = fingerprintProtection;
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return activated;
    }
}