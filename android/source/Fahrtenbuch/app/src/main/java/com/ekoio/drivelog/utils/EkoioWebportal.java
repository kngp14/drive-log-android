/*
 * EkoioWebportal
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.utils;

import android.content.Context;

import com.ekoio.drivelog.R;
import com.ekoio.drivelog.utils.tripinfos.TripInfos;
import com.ekoio.drivelog.utils.tripinfos.NewTrip;
import com.ekoio.drivelog.utils.tripinfos.TripsLogGet;
import com.ekoio.drivelog.utils.tripinfos.DriveLog;
import com.google.gson.Gson;

public class EkoioWebportal {

    public final static String LOGIN_SUCCESSFULL = "successfullLogin";
    public final static String LOGOUT_SUCCESSFULL = "successfullLogout";
    public final static String DRIVE_LOG_SUCCESSFULL = "successfullTripLog";
    public final static String NETWORK_ERROR = "UnknownHostException";

    /**
     * Anmeldung am ekoio Webservice und Rückgabe von Informationen zum Anmeldevorgang
     * @param context Aufrufende Activity (this)
     * @param account Anmeldeinformationen des anzumeldenden Nutzers
     * @param saveCredentials True - Zugangsdaten speichern
     * @return LOGIN_SUCCESSFULL oder Fehlermeldung
     */
    public String login(Context context, Account account, boolean saveCredentials) {
        String returnMessage = LOGIN_SUCCESSFULL;

        EkoioHttpClient loginHttpClient = new EkoioHttpClient(EkoioHttpClient.LOGIN_URL);
        Gson gson = new Gson();
        loginHttpClient.makeRequest(EkoioHttpClient.POST, gson.toJson(account, Account.class));

        if (loginHttpClient.requestSuccessfull() && loginHttpClient.getResult().contains("redirect_url")) {

            //Zugangsdaten speichern, wenn gewünscht
            CredentialStorageManager csm = new CredentialStorageManager(context);
            if (saveCredentials) {
                if (!csm.saveCredentials(account))
                    returnMessage = context.getString(R.string.msg_failed_save_credentials);
            } else {
                csm.deleteCredentials();
            }

            // Session-Cookie speichern für weitere Requests
            csm.saveCookie(loginHttpClient.getSessionCookie());

        } else if (!loginHttpClient.requestSuccessfull() && loginHttpClient.getResult().contains("email")) {
            try {
                int beginError = loginHttpClient.getResult().indexOf('[') + 1;
                int endError = loginHttpClient.getResult().indexOf(']') - 1;
                returnMessage = loginHttpClient.getResult().substring(beginError, endError);
            } catch (Exception e) {
                returnMessage = context.getString(R.string.msg_failed_login);
            }
        } else if(loginHttpClient.getResult().equals(EkoioHttpClient.NETWORK_ERROR)) {
            returnMessage = NETWORK_ERROR;
        } else {
            returnMessage = context.getString(R.string.msg_succeded_login);
        }

        return returnMessage;
    }

    /**
     * Anmeldung am ekoio Webservice und Rückgabe von Informationen zum Anmeldevorgang
     * @param context Aufrufende Activity (this)
     * @return LOGOUT_SUCCESSFULL oder Fehlermeldung
     */
    public String logout(Context context) {
        String returnMessage = LOGOUT_SUCCESSFULL;

        CredentialStorageManager csm = new CredentialStorageManager(context);
        String sessionCookie = csm.getCookie();

        EkoioHttpClient logoutHttpClient = new EkoioHttpClient(EkoioHttpClient.LOGOUT_URL);
        logoutHttpClient.makeRequest(EkoioHttpClient.POST,sessionCookie,  "");

        if(logoutHttpClient.requestSuccessfull() && logoutHttpClient.getResult().contains("success")) {

            // Zugangsdaten und Session-Cookie löschen
            csm.deleteCredentials();

            // Fingerabdruck-Absicherung löschen
            csm.deleteFingerprintProtection();

        } else {
            returnMessage = logoutHttpClient.getResult();
        }

        return returnMessage;
    }

    /**
     * Fahrtinformationen
     * @param tripID ID über Small-Trips
     * @return TripInfos
     */
    public TripInfos getTripInfos(Context context, String tripID) {
        TripInfos tripInfos;
        CredentialStorageManager csm = new CredentialStorageManager(context);
        String sessionCookie = csm.getCookie();

        EkoioHttpClient mileageHttpClient = new EkoioHttpClient(EkoioHttpClient.TRIP_INFOS_URL + tripID);
        mileageHttpClient.makeRequest(EkoioHttpClient.GET, sessionCookie);

        if(mileageHttpClient.requestSuccessfull()) {

            tripInfos = new Gson().fromJson(mileageHttpClient.getResult(), TripInfos.class);

        } else {
            tripInfos = null;
        }

        return tripInfos;
    }

    /**
     * GET Request an Drive Logs API für bereits kategorisierte Fahrten
     * @param context Aufrufende GUI/Context
     * @param driveLogid ID in Drive Log Instance
     * @return TripsLogGet mit travel_purpose etc.
     */
    public TripsLogGet getTripLog(Context context, String driveLogid) {
        TripsLogGet tripsLogGet;

        CredentialStorageManager csm = new CredentialStorageManager(context);
        String sessionCookie = csm.getCookie();

        EkoioHttpClient tripLogHttpClient = new EkoioHttpClient(EkoioHttpClient.DRIVE_LOGS_URL + driveLogid);
        tripLogHttpClient.makeRequest(EkoioHttpClient.GET, sessionCookie);

        if(tripLogHttpClient.requestSuccessfull() && tripLogHttpClient.getResult().contains("travel_purpose")) {
            tripsLogGet = new Gson().fromJson(tripLogHttpClient.getResult(), TripsLogGet.class);
        } else {
            tripsLogGet = null;
        }

        return tripsLogGet;
    }

    /**
     * PUT Request, um Drive Log Eintrag zu aktualisieren
     * @param context Aufrufende GUI/Context
     * @param tripLog TripsLogGet mit allen Informationen
     * @return DRIVE_LOG_SUCCESSFULL oder Fehlermeldung
     */
    public String updateDriveLog(Context context, DriveLog tripLog) {
        String returnMessage;

        CredentialStorageManager csm = new CredentialStorageManager(context);
        String sessionCookie = csm.getCookie();


        String url = EkoioHttpClient.DRIVE_LOGS_URL + tripLog.getId() + "/";
        EkoioHttpClient driveLogUpdateHttpClient = new EkoioHttpClient(url);
        driveLogUpdateHttpClient.makeRequest(EkoioHttpClient.PUT, sessionCookie, new Gson().toJson(tripLog));

        if(driveLogUpdateHttpClient.requestSuccessfull()) {
            returnMessage = DRIVE_LOG_SUCCESSFULL;
        } else {
            returnMessage = driveLogUpdateHttpClient.getResult();
        }

        return returnMessage;
    }

    /**
     * POST Request, um Drive Log Eintrag vor der Fahrt anzulegen
     * @param context Aufrufende GUI/Context
     * @param tripLog NewTrip mit Kategorie und Reisezweck/Notiz
     * @return DRIVE_LOG_SUCCESSFULL oder Fehlermeldung
     */
    public String createDriveLogForNextTrip(Context context, NewTrip tripLog) {
        String returnMessage;

        CredentialStorageManager csm = new CredentialStorageManager(context);
        String sessionCookie = csm.getCookie();


        String url = EkoioHttpClient.DRIVE_LOGS_URL;
        EkoioHttpClient driveLogUpdateHttpClient = new EkoioHttpClient(url);
        driveLogUpdateHttpClient.makeRequest(EkoioHttpClient.POST, sessionCookie, new Gson().toJson(tripLog));

        if(driveLogUpdateHttpClient.requestSuccessfull()) {
            returnMessage = DRIVE_LOG_SUCCESSFULL;
        } else {
            returnMessage = driveLogUpdateHttpClient.getResult();
        }

        return returnMessage;
    }

    /**
     * POST Request, um Drive Log Eintrag zu einer Fahrt anzulegen
     * @param context Aufrufende GUI/Context
     * @param driveLog DriveLog mit allen Informationen
     * @return DRIVE_LOG_SUCCESSFULL oder Fehlermeldung
     */
    public String createDriveLog(Context context, DriveLog driveLog) {
        String returnMessage;

        CredentialStorageManager csm = new CredentialStorageManager(context);
        String sessionCookie = csm.getCookie();

        String url = EkoioHttpClient.DRIVE_LOGS_URL;
        EkoioHttpClient driveLogHttpClient = new EkoioHttpClient(url);
        driveLogHttpClient.makeRequest(EkoioHttpClient.POST, sessionCookie, new Gson().toJson(driveLog));

        if(driveLogHttpClient.requestSuccessfull()) {
            returnMessage = DRIVE_LOG_SUCCESSFULL;
        } else {
            returnMessage = driveLogHttpClient.getResult();
        }

        return returnMessage;
    }
}