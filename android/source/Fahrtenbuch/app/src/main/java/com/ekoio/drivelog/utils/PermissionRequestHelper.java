/*
 * PermissionRequestHelper
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.utils;


import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.lang.reflect.Method;

public class PermissionRequestHelper {

    // Request ID zur Identifizierung des OnRequestResult
    private static final int PERMISSIONS_REQUEST_CODE = 100;

    // Activity Referenz für Reflection
    private Activity runningActivity;

    /**
     * Neue Berechtigung-Abfrage an Nutzer senden
     * @param runningActivity Aktuelle Activity
     * @param permission      Berechtigung
     */
    public void requestPermission(Activity runningActivity, String permission) {
        this.runningActivity = runningActivity;

        if (ContextCompat.checkSelfPermission(runningActivity,
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(runningActivity,
                    new String[]{permission},
                    PERMISSIONS_REQUEST_CODE);

        } else {
            callInterface(runningActivity, permission, true);
        }
    }


    /**
     * onRequestPermissionsResult der runningActivity durch Reflection abgefangen
     * @param requestCode  Request ID
     * @param permissions  Berechtigung
     * @param grantResults Auswertung der Berechtigungs-Anfrage
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (runningActivity != null) {
                        callInterface(runningActivity, permissions[0], true);
                    }

                } else {
                    if (runningActivity != null) {
                        callInterface(runningActivity, permissions[0], false);
                    }

                }
            }
        }
    }

    /**
     * onPermissionResult Aufruf der runningActivity durch Reflection
     * @param activity   Activity Referenz
     * @param permission Berechtigung
     * @param isGranted  true, wenn Berechtigung erteilt
     * @throws InterfaceNotImplementedException throws when OnPermissionResult is not implemented
     */
    private void callInterface(Activity activity, String permission, boolean isGranted) throws InterfaceNotImplementedException {
        Method method;
        try {
            method = activity.getClass().getMethod("onPermissionResult", String.class, boolean.class);
        } catch (NoSuchMethodException e) {
            throw new InterfaceNotImplementedException("PermissionRequestHelper.OnPermissionResult  ist nicht in der Activity mit Berechtigungsanfrage implementiert!");
        }
        if (method != null) {
            try {
                method.invoke(activity, permission, isGranted);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Interface um Auswertung der Berechitgungsanfrage zu starten
     * (muss in Acitvity, die Berechitigungen anfragen will, implementiert werden)
     */
    public interface OnPermissionResult {
        /**
         * Methode, die nach Berechtigungsanfrage aufgerufen wird
         * @param permission Berechtigung
         * @param isGranted  true, wenn Berechtigung erteilt
         */
        void onPermissionResult(String permission, boolean isGranted);
    }

    /**
     * Exception für nicht implementierte OnPermissionResult-Methode
     */
    private class InterfaceNotImplementedException extends RuntimeException {
        private InterfaceNotImplementedException(String message) {
            super(message);
        }
    }
}
