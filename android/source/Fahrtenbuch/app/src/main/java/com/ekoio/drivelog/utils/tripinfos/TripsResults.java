/*
 * Trips_Results
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.utils.tripinfos;

import com.ekoio.drivelog.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class TripsResults {
    private String url;
    private String id;
    private String first_datetime;
    private String first_time;
    private String last_datetime;
    private String last_time;
    private TripsVehicle vehicle;

    private TripsAddress start_address;
    private TripsAddress end_address;

    private double distance;
    private float avg_speed;
    private float level;

    private TripsDriveLog drive_log;
    private String export_url;

    /**
     * URL der Fahrt
     * @return URL
     */
    public String getUrl() { return url; }

    /**
     * ID der Fahrt
     * @return ID
     */
    public String getID() { return id; }

    /**
     * Startdatum und -zeit der Fahrt
     * @return Startdatum
     */
    public String getFirstDateTime() { return first_datetime; }

    /**
     * Enddatum und -zeit der Fahrt
     * @return Enddatum
     */
    public String getLastDateTime() { return last_datetime; }

    /**
     * Fahrzeug der Fahrt
     * @return Fahrzeug
     */
    public TripsVehicle getVehicle() { return vehicle; }


    /**
     * Startadresse der Fahrt
     * @return Startadresse
     */
    public TripsAddress getStartAddress() {
        return start_address;
    }

    /**
     * Endadresse der Fahrt
     * @return Endadresse
     */
    public TripsAddress getEndAddress() {
        return end_address;
    }

    /**
     * Distanz der Fahrt
     * @return Distanz
     */
    public Double getDistance() {
        return distance;
    }

    /**
     * Durchschnittsgeschwindigkeit der Fahrt
     * @return Durchschnittsgeschwindigkeit
     */
    public Float getAvgSpeed() {
        return avg_speed;
    }

    /**
     * Level der Fahrt
     * @return Level
     */
    public Float getLevel() {
        return level;
    }

    /**
     * Art der Fahrt
     * @return Log
     */
    public TripsDriveLog getDriveLog() {
        return drive_log;
    }

    /**
     * Export-URL der Fahrt
     * @return URL
     */
    public String getExportURL() {
        return export_url;
    }


    /**
     * Startdatum der Fahrt
     * @return Datum
     */
    public String getFormattedDate() {
        String dateString = "Unbekannt";
        String utcDateTime = getFirstDateTime();

        try {
            DateFormat utc = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            utc.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utc.parse(utcDateTime);

            DateFormat localeDate = new SimpleDateFormat("dd. MMMM yyyy");
            localeDate.setTimeZone(TimeZone.getDefault());

            dateString = localeDate.format(date);

        } catch (ParseException e) {}

        return dateString;
    }

    /*
    * Extrahieren der Uhrzeit aus Datum+Uhrzeit
    * @return HH::MM Uhr
    */
    public String getFormattedStartTime() {
        String timeString = "Unbekannt";
        String utcDateTime = getFirstDateTime();
        try {
            DateFormat utc = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            utc.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utc.parse(utcDateTime);

            DateFormat localeTime = new SimpleDateFormat("HH:mm");
            localeTime.setTimeZone(TimeZone.getDefault());

            timeString = localeTime.format(date) + " " + "Uhr";

        } catch (ParseException e) {}
        return timeString;
    }


    /**
     * Extrahieren der Uhrzeit aus Datum+Uhrzeit
     * @return HH::MM Uhr
     */
    public String getFormattedEndTime() {
        String timeString = "Unbekannt";
        String utcDateTime = getLastDateTime();
        try {
            DateFormat utc = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            utc.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utc.parse(utcDateTime);

            DateFormat localeTime = new SimpleDateFormat("HH:mm");
            localeTime.setTimeZone(TimeZone.getDefault());

            timeString = localeTime.format(date) + " " + "Uhr";

        } catch (ParseException e) {}
        return timeString;
    }

    /**
     * zurückgelegte Distanz
     * @return Distanz
     */
    public String getFormattedDistance() {
        // Umrechnung Meter in Kilometer
        distance = distance/1000;

        // Formatierung, sodass nur 1 Nachkommastelle vorhanden ist
        String formattedDistance = String.format("%.1f", distance);
        formattedDistance.replace(",", ".");
        return formattedDistance;
    }




}
