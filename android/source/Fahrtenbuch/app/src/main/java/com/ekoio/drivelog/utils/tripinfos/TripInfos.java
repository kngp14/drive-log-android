/*
 * TripMileage
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.utils.tripinfos;

/**
 * Kilometerstand (mit ekoio Stick getrackte Kilometer)
 */
public class TripInfos {
    private int start_mileage;
    private int end_mileage;
    private String start_datetime;
    private String end_datetime;
    private String start_address;
    private String end_address;
    private int distance;
    private String drive_log_pk;
    private int trip;

    /**
     * ekoio-Kilometerstand nach der Fahrt
     * @return Kilometerstand in Meter
     */
    public int getEndMileage() {
        return end_mileage;
    }

    /**
     * ekoio-Kilometerstand vor der Fahrt
     * @return Kilometerstand in Meter
     */
    public int getStartMileage() {
        return start_mileage;
    }

    /**
     * Datum und Uhrzeit zu Fahrtbeginn
     * @return YYYY-MM-DDTHH:MM:SSZ
     */
    public String getStartDateTime() {
        return start_datetime;
    }

    /**
     * Datum und Uhrzeit zum Fahrtende
     * @return YYYY-MM-DDTHH:MM:SSZ
     */
    public String getEndDateTime() {
        return end_datetime;
    }

    /**
     * Straße und Hausnummer der Start-Adresse
     * @return "Straße Hausnummer"
     */
    public String getStartStreet() {
        return start_address.split(", ")[0];
    }

    /**
     * Postleitzahl und Ort der Start-Adresse
     * @return "Postleitzahl Ort"
     */
    public String getStartCity() {
        return start_address.split(", ")[1];
    }

    /**
     * Straße und Hausnummer der Ziel-Adresse
     * @return "Straße Hausnummer"
     */
    public String getEndStreet() {
        return end_address.split(", ")[0];
    }

    /**
     * Postleitzahl und Ort der Ziel-Adresse
     * @return "Postleitzahl Ort"
     */
    public String getEndCity() {
        return end_address.split(", ")[1];
    }

    /**
     * Gefahrene Strecke
     * @return Meter
     */
    public int getDistance() {
        return distance;
    }

    /**
     * Fahrtenbuch-ID
     * @return ID oder "null" (String)
     */
    public String getDriveLogID() {
        return drive_log_pk;
    }

    /**
     * Fahrt-ID
     * @return ID
     */
    public int getTripID() {
        return trip;
    }
}
