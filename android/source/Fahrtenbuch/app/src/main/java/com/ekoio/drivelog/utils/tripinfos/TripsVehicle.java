/*
 * Trips_Vehicle
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


package com.ekoio.drivelog.utils.tripinfos;

public class TripsVehicle {
    private String id;
    private String vin;
    private String name;
    private String customers_id;
    private String license_plate;
    private String[] tags;
    private String[] vehicle_events;


    /**
     * ID des Fahrzeugs
     * @return ID
     */
    public String getId() {return id;}

    /**
     * VIN des Fahrzeugs
     * @return VIN
     */
    public String vin() {return vin;}

    /**
     * Name des Fahrzeugs
     * @return Name
     */
    public String getName() {return name;}

    /**
     * Kunden-ID
     * @return ID
     */
    public String getCustomersId() {return customers_id;}

    /**
     * Kennzeichen des Fahrzeugs
     * @return Kennzeichen
     */
    public String getLicensePlate() {return license_plate;}

    /**
     * Tags der Fahrt
     * @param index der Fahrt
     * @return Tags
     */
    public String getTags(int index) {return tags[index];}

    /**
     * Fahrzeugevents
     * @param index der Fahrt
     * @return Events
     */
    public String getVehicleEvents(int index) {return vehicle_events[index];}
}
