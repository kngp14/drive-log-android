/*
 * TripsLogGet
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.utils.tripinfos;

public class TripsLogGet {
    private Integer id;
    private int trip;
    private int type;
    private String refuelled_volume;
    private String travel_purpose;
    private String start_datetime;
    private String end_datetime;
    private int distance;
    private String start_address;
    private String end_address;
    private int start_mileage;
    private int end_mileage;

    public TripsLogGet() {

    }

    /**
     * TripsLogGet Objekt zum Erstellen eines Eintrags in der Drive Logs API
     * @param type
     * @param start_datetime
     * @param end_datetime
     * @param distance
     * @param start_address
     * @param end_address
     * @param trip
     * @param travel_purpose
     * @param refuelled_volume
     * @param start_mileage
     * @param end_mileage
     */
    public TripsLogGet(int type,
                       String start_datetime,
                       String end_datetime,
                       int distance,
                       String start_address,
                       String end_address,
                       int trip,
                       String travel_purpose,
                       String refuelled_volume,
                       int start_mileage,
                       int end_mileage) {
        this.type = type;
        this.start_datetime = start_datetime;
        this.end_datetime = end_datetime;
        this.distance = distance;
        this.start_address = start_address;
        this.end_address = end_address;
        this.trip = trip;
        this.travel_purpose = travel_purpose;
        this.refuelled_volume = refuelled_volume;
        this.start_mileage = start_mileage;
        this.end_mileage = end_mileage;
    }

    public Integer getId() {
        return id;
    }

    public int getTrip() {
        return trip;
    }

    public int getType() {
        return type;
    }

    public String getRefuelledVolume() {
        return refuelled_volume;
    }

    public String getTravelPurpose() {
        return travel_purpose;
    }

    public String getStartDateTime() {
        return start_datetime;
    }

    public String getEndDateTime() {
        return end_datetime;
    }

    public int getDistance() {
        return distance;
    }

    public String getStartAddress() {
        return start_address;
    }

    public String getEndAddress() {
        return end_address;
    }

    public int getStartMileage() {
        return start_mileage;
    }

    public int getEndMileage() {
        return end_mileage;
    }
}
