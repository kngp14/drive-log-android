/*
 * TripsLogGetTest
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.utils.tripinfos;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.fail;
import static org.junit.Assert.assertEquals;

public class TripsLogGetTest {

    private TripsLogGet testTripsLogGet;

    private Integer test_id;
    private int test_trip;
    private int test_type;
    private String test_refuelled_volume;
    private String test_travel_purpose;
    private String test_start_datetime;
    private String test_end_datetime;
    private int test_distance;
    private String test_start_address;
    private String test_end_address;
    private int test_start_mileage;
    private int test_end_mileage;

    @Before
    public void setUp() {

        // Dummy-Werte
        // this.test_id = null; // Feld wird in Original-Objekt nie verwendet
        this.test_type = 2;
        this.test_start_datetime = "20161208-163000";
        this.test_end_datetime = "20161208-183000";
        this.test_distance = 120;
        this.test_start_address = "Startstrasse";
        this.test_end_address = "Zielstrasse";
        this.test_trip = 3;
        this.test_travel_purpose = "dienstlich";
        this.test_refuelled_volume = "65";
        this.test_start_mileage = 32500;
        this.test_end_mileage = 32620;

        this.testTripsLogGet = new TripsLogGet( this.test_type,
                this.test_start_datetime,
                this.test_end_datetime,
                this.test_distance,
                this.test_start_address,
                this.test_end_address,
                this.test_trip,
                this.test_travel_purpose,
                this.test_refuelled_volume,
                this.test_start_mileage,
                this.test_end_mileage );
    }

  // Derzeit wird keine Exception geworfen (nur Beispiel)
  // @Test
  // public void testTripsLogGetConstructorIllegalValue() {
  //     try {
  //         this.testTripsLogGet = new TripsLogGet(0, null, null, 0, null, null, 0, null, null, 0, 0);
  //         fail("Exception was expected for null input");
  //     } catch (IllegalArgumentException e) {
  //
  //      }
  // }

   // @Test
   // public void testIdGetter() {
   //
   //    assertEquals( this.testTripsLogGet.getId(), this.test_id );
   //
   // }

    @Test
    public void testTypeGetter() {

        assertEquals( this.testTripsLogGet.getType(), this.test_type );

    }

    @Test
    public void testStartDateTimeGetter() {

        assertEquals( this.testTripsLogGet.getStartDateTime(), this.test_start_datetime );

    }

    @Test
    public void testEndDateTimeGetter() {

        assertEquals( this.testTripsLogGet.getEndDateTime(), this.test_end_datetime );

    }

    @Test
    public void testDistanceGetter() {

        assertEquals( this.testTripsLogGet.getDistance(), this.test_distance );

    }

    @Test
    public void testStartAddressGetter() {

        assertEquals( this.testTripsLogGet.getStartAddress(), this.test_start_address );

    }

    @Test
    public void testEndAddressGetter() {

        assertEquals( this.testTripsLogGet.getEndAddress(), this.test_end_address );

    }

    @Test
    public void testTripGetter() {

        assertEquals( this.testTripsLogGet.getTrip(), this.test_trip );

    }

    @Test
    public void testTravelPurposeGetter() {

        assertEquals( this.testTripsLogGet.getTravelPurpose(), this.test_travel_purpose );

    }

    @Test
    public void testRefuelledVolumeGetter() {

        assertEquals( this.testTripsLogGet.getRefuelledVolume(), this.test_refuelled_volume );

    }

    @Test
    public void testStartMileageGetter() {

        assertEquals( this.testTripsLogGet.getStartMileage(), this.test_start_mileage );

    }

    @Test
    public void testEndMileageGetter() {

        assertEquals( this.testTripsLogGet.getEndMileage(), this.test_end_mileage );

    }



}