/*
 * TripCreatorActivity
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.ui;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.Toast;

import com.ekoio.drivelog.R;
import com.ekoio.drivelog.ui.view.WaveView;
import com.ekoio.drivelog.utils.EkoioWebportal;
import com.ekoio.drivelog.utils.Logger;
import com.ekoio.drivelog.utils.tripinfos.NewTrip;

public class TripCreatorActivity extends AppCompatActivity {

    private AppCompatButton btnSave;
    private AppCompatButton btnCancel;
    private ProgressBar progrSave;
    private RadioGroup tglCategory;
    private EditText etPurpose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_creator);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);

        bindUI();
        initCancelButton();
        initSaveButton();
    }

    /**
     * UI Elemente einlesen
     */
    public void bindUI() {
        btnSave = (AppCompatButton) findViewById(R.id.ac_trip_cre_bt_save);
        btnCancel = (AppCompatButton) findViewById(R.id.ac_trip_cre_bt_cancel);
        progrSave = (ProgressBar) findViewById(R.id.ac_trip_cre_progress_save);
        tglCategory = (RadioGroup) findViewById(R.id.ac_trip_cre_toggle_category);
        etPurpose = (EditText) findViewById(R.id.ac_trip_cre_et_purpose);

        RadioButton rbtnBusiness = (RadioButton) findViewById(R.id.ac_trip_cre_toggle_category_business);
        rbtnBusiness.setText(getResources().getTextArray(R.array.categories)[0].toString().toUpperCase());
        RadioButton rbtnPrivate = (RadioButton) findViewById(R.id.ac_trip_cre_toggle_category_private);
        rbtnPrivate.setText(getResources().getTextArray(R.array.categories)[1].toString().toUpperCase());

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            RelativeLayout headerLayout = (RelativeLayout) findViewById(R.id.ac_trip_cre_header_layout);
            headerLayout.setBackgroundColor(getResources().getColor(R.color.colorHeader));
            Space space = (Space) findViewById(R.id.ac_trip_cre_space);
            space.setVisibility(View.GONE);

            WaveView header = (WaveView) findViewById(R.id.ac_trip_cre_header);
            header.setVisibility(View.INVISIBLE);

        }
    }

    /**
     * Abbrechen-Button konfigurieren
     */
    public void initCancelButton() {
        this.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(RESULT_CANCELED, returnIntent);
                finish();
            }
        });
        //noinspection RestrictedApi
        this.btnCancel.setSupportBackgroundTintList(getResources().getColorStateList(R.color.colorWhite));
    }

    /**
     * Speichern-Button konfigurieren
     */
    public void initSaveButton() {
        this.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDriveLog();
            }
        });
        //noinspection RestrictedApi
        this.btnSave.setSupportBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
    }

    /**
     * Ladeanimation ein/ausblenden und Anmeldemaske ausblenden
     * @param show true zum Einblenden
     */
    private void showProgress(final boolean show) {
        String buttonLoginText;
        int colorText;
        int progressVisibility;
        if(show) {
            buttonLoginText = "";
            colorText = getResources().getColor(R.color.colorDarkGray);
            progressVisibility = View.VISIBLE;
            //noinspection RestrictedApi
            this.btnSave.setSupportBackgroundTintList(getResources().getColorStateList(R.color.colorGray));
        } else {
            buttonLoginText = "SPEICHERN";
            colorText = getResources().getColor(R.color.colorWhite);
            progressVisibility = View.GONE;
            //noinspection RestrictedApi
            this.btnSave.setSupportBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
        }
        this.btnSave.setTextColor(colorText);
        this.btnSave.setText(buttonLoginText);
        this.progrSave.setVisibility(progressVisibility);
        this.progrSave.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorDarkGray), android.graphics.PorterDuff.Mode.MULTIPLY);
        this.btnSave.setEnabled(!show);
    }

    /**
     * Hochladen des Fahrtbuchs mit Typ, getankten Liter und Reisezweck/Notiz
     */
    private void createDriveLog() {
        class DriveLogCreator extends AsyncTask<Object, Void, String> {

            Context context;
            NewTrip tripLog;

            public DriveLogCreator(Context context, NewTrip tripLog) {
                this.context = context;
                this.tripLog = tripLog;
            }

            @Override
            protected void onPreExecute() {
                showProgress(true);
            }

            @Override
            protected String doInBackground(Object... params) {
                EkoioWebportal ekoioWebportal = new EkoioWebportal();
                String returnMessage = ekoioWebportal.createDriveLogForNextTrip(this.context, this.tripLog);
                return returnMessage;
            }

            @Override
            protected void onPostExecute(String returnMessage) {
                if(returnMessage.equals(EkoioWebportal.DRIVE_LOG_SUCCESSFULL)) {
                    Intent returnIntent = new Intent();
                    setResult(RESULT_OK, returnIntent);
                    finish();
                } else {
                    Toast.makeText(
                            this.context,
                            getString(R.string.msg_tray_again_later) + returnMessage,
                            Toast.LENGTH_SHORT).show();
                    Logger.log(this.context.getClass(), "Fehler beim Anlegen der Fahrt: " + returnMessage);
                    showProgress(false);
                }
            }
        }

        RadioButton checkedRadioButton = (RadioButton) findViewById(this.tglCategory.getCheckedRadioButtonId());
        int type = Integer.parseInt(checkedRadioButton.getImeActionLabel().toString());
        Logger.log(this.getClass(), "type = " + type);


        String travelPurpose = this.etPurpose.getText().toString();
        if(travelPurpose.equals("")) travelPurpose = "";
        Logger.log(this.getClass(), "travelPurpose = " + travelPurpose);

        NewTrip tripLog = new NewTrip(type, travelPurpose);

        DriveLogCreator driveLogCreator = new DriveLogCreator(this, tripLog);
        driveLogCreator.execute(this);
    }
}
