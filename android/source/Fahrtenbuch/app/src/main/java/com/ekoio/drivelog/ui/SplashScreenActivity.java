/*
 * SplashScreenActivity
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.ui;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ekoio.drivelog.R;
import com.ekoio.drivelog.utils.Account;
import com.ekoio.drivelog.utils.CredentialStorageManager;
import com.ekoio.drivelog.utils.EkoioWebportal;
import com.ekoio.drivelog.utils.Logger;
import com.ekoio.drivelog.utils.PermissionRequestHelper;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * Splash-Screen mit ekoio Lade-Animation
 * Author: Kevin Neumann
 */
public class SplashScreenActivity extends AppCompatActivity implements PermissionRequestHelper.OnPermissionResult {
    public static final String FRIST_RUN = "FirstRun";
    private Intent intent; // LoginActivity oder MainActivity

    private FingerprintManager fingerprintManager;
    private PermissionRequestHelper permissionRequestHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        GifImageView gifImageView = (GifImageView) findViewById(R.id.act_splashscreen_gifImageView);
        GifDrawable gifDrawable = (GifDrawable) gifImageView.getDrawable(); // 1690ms, 44 Frames
        gifDrawable.setSpeed(0.7f);
        gifDrawable.setLoopCount(1);
        gifDrawable.stop();

        /** Berechtigung für Internetverbindung **/
        permissionRequestHelper = new PermissionRequestHelper();
        permissionRequestHelper.requestPermission(this, Manifest.permission.INTERNET);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionRequestHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionResult(final String permission, boolean isGranted) {
        switch (permission) {
            case Manifest.permission.INTERNET:
                if (isGranted) {
                    Logger.log(this.getClass(), "Berechtigung INTERNET erhalten");

                    // Weitere Berechtigung für ACCESS_NETWORK_STATE anfordern
                    permissionRequestHelper.requestPermission(SplashScreenActivity.this, Manifest.permission.ACCESS_NETWORK_STATE);
                } else {
                    Logger.log(this.getClass(), "Berechtigung INTERNET verweigert");

                    // Fehlermeldung und erneute Berechtigungsanforderung
                    new MaterialDialog.Builder(SplashScreenActivity.this)
                            .title("Berechtigung INTERNET verweigert")
                            .content("Ohne die Berechtigung zur Verbindung mit dem Internet, funktioniert die App nicht. Sie können die Berechtigungs-Anfrage erneut anzeigen lassen oder die App beenden.")
                            .positiveText("ERNEUT VERSUCHEN")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    permissionRequestHelper.requestPermission(SplashScreenActivity.this, permission);
                                }
                            })
                            .negativeText(R.string.close_app)
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    finish();
                                }
                            })
                            .show();
                }
                break;

            case Manifest.permission.ACCESS_NETWORK_STATE:
                if (isGranted) {
                    Logger.log(this.getClass(), "Berechtigung ACCESS_NETWORK_STATE erhalten");

                    /** Fingerabdrucksensor ab Android Marshmallow möglich **/
                    CredentialStorageManager csm = new CredentialStorageManager(this);
                    if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) && (csm.getFingerprintProtection().equals("true"))) {

                        permissionRequestHelper.requestPermission(SplashScreenActivity.this, Manifest.permission.USE_FINGERPRINT);

                    } else {
                        new SplashBackgroundTask(this).execute();
                    }
                } else {
                    Logger.log(this.getClass(), "Berechtigung ACCESS_NETWORK_STATE verweigert");

                    // Fehlermeldung und erneute Berechtigungsanforderung
                    new MaterialDialog.Builder(SplashScreenActivity.this)
                            .title("Berechtigung ACCESS_NETWORK_STATE verweigert")
                            .content("Ohne die Berechtigung zur Verbindung mit dem Internet, funktioniert die App nicht. Sie können die Berechtigungs-Anfrage erneut anzeigen lassen oder die App beenden.")
                            .positiveText("ERNEUT VERSUCHEN")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    permissionRequestHelper.requestPermission(SplashScreenActivity.this, permission);
                                }
                            })
                            .negativeText(R.string.close_app)
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    finish();
                                }
                            })
                            .show();
                }

                break;

            case Manifest.permission.USE_FINGERPRINT:
                if (isGranted) {
                    Logger.log(this.getClass(), "Berechtigung USE_FINGERPRINT erhalten");

                    fingerprintManager = (FingerprintManager) getSystemService(Context.FINGERPRINT_SERVICE);
                    if (fingerprintManager.isHardwareDetected() && fingerprintManager.hasEnrolledFingerprints()) {

                        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                                .customView(R.layout.component_dialog_fingerprint, true)
                                .cancelable(false);

                        MaterialDialog dialog = builder.build();
                        dialog.show();
                        activateFingerprintScanner(dialog, this);
                    }

                } else {
                    Logger.log(this.getClass(), "Berechtigung USE_FINGERPRINT verweigert");

                    // Fehlermeldung
                    new MaterialDialog.Builder(SplashScreenActivity.this)
                            .title("Berechtigung USE_FINGERPRINT verweigert")
                            .content("App-Sicherheitseinstellung sieht jedoch Fingerabdruck-Scan vor! Gewähren Sie die Berechtigung oder installieren Sie die App neu, um die Sicherheits-Konfiguration zurückzusetzen.")
                            .positiveText("ERNEUT VERSUCHEN")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    permissionRequestHelper.requestPermission(SplashScreenActivity.this, permission);
                                }
                            })
                            .negativeText("DEINSTALLIEREN")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    Uri packageUri = Uri.parse("package:com.ekoio.drivelog");
                                    Intent uninstallIntent = new Intent(Intent.ACTION_DELETE , packageUri);
                                    startActivity(uninstallIntent);
                                }
                            })
                            .show();
                }
                break;
        }
    }

    private class SplashBackgroundTask extends AsyncTask<Void, Void, String> {
        private Context context;
        private GifDrawable gifDrawable;

        public SplashBackgroundTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initGif();
        }

        @Override
        protected String doInBackground(Void... params) {
            String returnMessage = FRIST_RUN;

            // Zugangsdaten laden und einloggen, falls vorhanden
            SharedPreferences sp = getSharedPreferences("DriveLog", MODE_PRIVATE);
            if(sp.getBoolean(CredentialStorageManager.SETTINGS_SAVEDCREDENTIALS, false)) {

                CredentialStorageManager csm = new CredentialStorageManager(this.context);
                Account account = csm.getCredentials();
                if(!account.getPassword().equals("ERROR") && !account.getPassword().equals("EMPTY")) {
                    // Login ausführen
                    EkoioWebportal ekoioWebportal = new EkoioWebportal();
                    returnMessage = ekoioWebportal.login(
                            this.context,
                            account,
                            true);

                } else if(account.getPassword().equals("ERROR")) {
                    csm.deleteCredentials();
                }
            }

            // Falls Login schneller als Ladeanimation, Animation erst abschließen bevor MainActivity
            while(!gifDrawable.isAnimationCompleted()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return returnMessage;
        }

        @Override
        protected void onPostExecute(String returnMessage) {

            intent = new Intent(SplashScreenActivity.this, LoginActivity.class);

            if(returnMessage.equals(EkoioWebportal.LOGIN_SUCCESSFULL)) {
                intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else if(returnMessage.equals(FRIST_RUN)) {
                startActivity(intent);
                finish();
            } else if(returnMessage.equals(EkoioWebportal.NETWORK_ERROR)) {
                new MaterialDialog.Builder(this.context)
                        .title(getString(R.string.dialog_title_failed_internet_connection))
                        .content(R.string.activity_splashscreen_dialog_check_internet_connection)
                        .positiveText(getString(R.string.dialog_positive_failed_internet_connection))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                                startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                            }
                        })
                        .negativeText(R.string.close_app)
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                                finish();
                            }
                        })
                        .show();
            } else {
                Toast.makeText(this.context,
                        R.string.msg_failed_login_no_internet,
                        Toast.LENGTH_SHORT).show();
                startActivity(intent);
                finish();
            }
        }

        /**
         * ekoio Ladeanimation initialisieren und starten
         */
        private void initGif() {
            GifImageView gifImageView = (GifImageView) findViewById(R.id.act_splashscreen_gifImageView);
            gifDrawable = (GifDrawable) gifImageView.getDrawable();
            gifDrawable.start();
            gifImageView.setOnClickListener(new View.OnClickListener() {
                int i = 0;
                @Override
                public void onClick(View v) {
                    if(i == 8) {
                        Toast.makeText(getApplicationContext(),
                                new String(CredentialStorageManager.KEY_BYTES),
                                Toast.LENGTH_SHORT).show();
                        i = 0;
                    }
                    i++;
                }
            });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 0) {
            startActivity(intent);
            finish();
        }
    }

    public void activateFingerprintScanner(final MaterialDialog dialog, final Context context) {
        FingerprintManager.AuthenticationCallback callback = new FingerprintManager.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errMsgId, CharSequence errString) {
                Toast.makeText(getApplicationContext(), getString(R.string.msg_wrong_fingerprint) +  "\n" + errString, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }

            @Override
            public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
                Toast.makeText(getApplicationContext(), helpString, Toast.LENGTH_SHORT).show();

                new AsyncTask<Void, Void, Void>() {

                    ImageView imageView = (ImageView) dialog.getCustomView().findViewById(R.id.comp_dialog_finger_image);

                    protected void onPreExecute() {
                        imageView.setColorFilter(getResources().getColor(R.color.colorOrange), PorterDuff.Mode.SRC_IN);

                    }
                    protected Void doInBackground(Void... unused) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    protected void onPostExecute(Void unused) {
                        imageView.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    }
                }.execute();
            }

            @Override
            public void onAuthenticationFailed() {
                Toast.makeText(getApplicationContext(), getString(R.string.msg_wrong_fingerprint), Toast.LENGTH_SHORT).show();

                new AsyncTask<Void, Void, Void>() {

                    ImageView imageView = (ImageView) dialog.getCustomView().findViewById(R.id.comp_dialog_finger_image);

                    protected void onPreExecute() {
                        imageView.setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_IN);

                    }
                    protected Void doInBackground(Void... unused) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    protected void onPostExecute(Void unused) {
                        imageView.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    }
                }.execute();
            }

            @Override
            public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

                new AsyncTask<Void, Void, Void>() {
                    protected void onPreExecute() {
                        ImageView imageView = (ImageView) dialog.getCustomView().findViewById(R.id.comp_dialog_finger_image);
                        imageView.setColorFilter(getResources().getColor(R.color.colorGreen), PorterDuff.Mode.SRC_IN);

                    }
                    protected Void doInBackground(Void... unused) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                    protected void onPostExecute(Void unused) {

                        dialog.dismiss();

                        // Ladeanimation starten und Anmeldung durchführen
                        new SplashBackgroundTask(context).execute();
                    }
                }.execute();


            }
        };
        fingerprintManager.authenticate(null, new CancellationSignal(), 7, callback, null);
    }
}
