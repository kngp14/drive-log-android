/*
 * MainActivity
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.ui;

import com.ekoio.drivelog.utils.Logger;
import com.ekoio.drivelog.utils.PermissionRequestHelper;
import com.ekoio.drivelog.utils.tripinfos.Trips;
import com.google.gson.Gson;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;

import com.ekoio.drivelog.R;
import com.ekoio.drivelog.utils.CredentialStorageManager;
import com.ekoio.drivelog.utils.EkoioHttpClient;
import com.ekoio.drivelog.utils.ListAdapter;
import com.ekoio.drivelog.utils.ListEntries;
import com.ekoio.drivelog.utils.ToolbarOnMenuItemListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.DatePickerDialog.OnDateSetListener;
import android.widget.TextView;
import android.widget.Toast;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;
import static android.os.Build.VERSION_CODES.M;

public class MainActivity extends Activity implements View.OnClickListener{

    // Zwischenspeicherung der Werte
    String[] dateEntries;
    String[] distanceEntries;
    String[] startAddressEntries;
    String[] endAddressEntries;
    String[] statusEntries;
    String[] startTimeEntries;
    String[] endTimeEntries;
    String[] tripID;
    String tripStatus = null;
    int rowCounter;

    // Definierung von notwendigen Variablen
    ListAdapter adapter;
    String startDate = "null";
    String endDate;
    Context  mContext;
    int newTripsCount = 0;
    boolean newTripsCountAdded = false;
    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");

    private TextView fromDateText;
    private TextView toDateText;
    private ImageView calendar1;
    private ImageView calendar2;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;

    private SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);
        toolbar.inflateMenu(R.menu.menu_main);
        ToolbarOnMenuItemListener onMenuItemClickListener = new ToolbarOnMenuItemListener(this);
        toolbar.setOnMenuItemClickListener(onMenuItemClickListener);

        // DateFormatter intitialiseren für Datumsfilter
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        // Views auslesen
        findViewsById();

        // Initialisierung der Kalenderdialoge
        setDateTimeField();

        // bei Klick auf Plus-Button reagieren und neue Activity aufrufen
        FloatingActionButton fabNewTrip = (FloatingActionButton) findViewById(R.id.ac_main_fab_new_trip);
        fabNewTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createTripIntent = new Intent(v.getContext(), TripCreatorActivity.class);
                ((Activity) v.getContext()).startActivityForResult(createTripIntent, 1);
            }
        });


        // bei Klick auf Kalendersymbol ebenfalls Kalender öffnen
        calendar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDateText.performClick();
            }
        });
        calendar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDateText.performClick();
            }
        });

        // Daten abfragen und bevor abgeschlossen Liste aufbauen
        new TripsLoadingTask(this).execute((Void) null);

        initPullToRefresh();
    }


    /**
     * Refresh, wenn Activity nach unten gezogen wird
     */
    public void initPullToRefresh() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                update();
            }
        });
        // Ladeanimation beim Start anzeigen, da Datenabfrage bei OnCreate gestartet wird
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            Toast.makeText(this, R.string.loading_trips, Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View view) {
        if (view == fromDateText) {
            fromDatePickerDialog.show();
        } else if (view == toDateText) {
            toDatePickerDialog.show();
        }
    }

    /**
     * Asynchron Liste der Fahrten abfragen und darstellen
     */
    private class TripsLoadingTask extends AsyncTask<Void, Void, String> {


        TripsLoadingTask(Context context) {
            mContext = context;
        }

        @Override
        protected String doInBackground(Void... params) {
            String returnMessage = listGetTrips(mContext);
            return returnMessage;
        }

        @Override
        protected void onPostExecute(String s) {
            if(!s.equals("OK"))
                Toast.makeText(mContext, s, Toast.LENGTH_SHORT).show();
            createList(mContext);

            // Ladeanimation nach Fertigstellung ausblenden
            if(swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        }
    }

    /**
     * neues Laden der Daten
     */
    public void update() {
        new TripsLoadingTask(this).execute((Void) null);
    }

    @Override
    /**
     * Auswertung Abbruch oder Speichern aus EditorActivity
     * @param requestCode
     * @param resultCode Ergebnis aus EditorActivity, abhängig welcher Button gedrückt wurde
     * @param data
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        swipeRefreshLayout.setRefreshing(true);
                    } else {
                        Toast.makeText(this, R.string.loading_trips, Toast.LENGTH_LONG).show();
                    }
                    update();
                    break;
                case Activity.RESULT_CANCELED:
                    break;
                default:
                    break;
            }
    }

    /**
     * Erstellung Dialoge und Auswertung Daten für Datumsfilter
     */
    private void setDateTimeField() {
        // Listener setzen
        fromDateText.setOnClickListener(this);
        toDateText.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        // Dialoge erstellen um Datum auszuwählen
        fromDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                startDate = dateFormatter.format(newDate.getTime());
                fromDateText.setText(startDate);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    swipeRefreshLayout.setRefreshing(true);
                } else {
                    Toast.makeText(getApplication(), R.string.loading_trips, Toast.LENGTH_SHORT).show();
                }
                update();
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());

        toDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                endDate = dateFormatter.format(newDate.getTime());
                toDateText.setText(endDate);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    swipeRefreshLayout.setRefreshing(true);
                } else {
                    Toast.makeText(getApplicationContext(), R.string.loading_trips, Toast.LENGTH_SHORT).show();
                }
                update();
            }

         }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        toDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
    }

    /**
     * Benötigte Views auselesen und zuweisen
     */
    private void findViewsById() {
        fromDateText    = (TextView) findViewById(R.id.ac_main_textView_fromDate);
        calendar1       = (ImageView) findViewById(R.id.ic_calendar1);
        calendar2       = (ImageView) findViewById(R.id.ic_calendar2);
        toDateText      = (TextView) findViewById(R.id.ac_main_textView_toDate);
    }

    /**
     * Erstellung der Liste mit gegebenen Werten
     * Werte für Datum und Anzahl neuer Fahrten zuweisen
     * @param mContext
     */
    public void createList(Context mContext) {
        // aufbauen der Listenelemente und mit Werte befüllen
        ListView list=(ListView) findViewById(R.id.ac_main_listView_trips);
        ArrayList<ListEntries> arrayOfListItems = new ArrayList<ListEntries>();
        if(newTripsCount != 0) {
            adapter = new ListAdapter(mContext, arrayOfListItems);
            String newTrips = String.format(getString(R.string.numberOfCreatedTrips), newTripsCount);
            ListEntries newTripsCountEntry = new ListEntries("", "", "", newTrips, "", "", "", "");
            adapter.add(newTripsCountEntry);
            list.setAdapter(adapter);
        }
        for (int i = 1; i <= rowCounter; i++) {
            adapter = new ListAdapter(mContext, arrayOfListItems);
            ListEntries newListEntries = new ListEntries(startAddressEntries[i-1], endAddressEntries[i-1], distanceEntries[i-1],
                    dateEntries[i-1], statusEntries[i-1], startTimeEntries[i-1], endTimeEntries[i-1], tripID[i-1]);
            adapter.add(newListEntries);
            list.setAdapter(adapter);
        }

        list.invalidateViews();

        // Umwandlung in anzuzeigendes Startdatumsformat
        if (Character.toString(startDate.charAt(2)).equals("-")) {
            fromDateText.setText(startDate);
        } else {
            try {
                Date date = dateFormat1.parse(startDate);
                startDate = dateFormat2.format(date);
            } catch (ParseException e) {
                Logger.log(this.getClass(), e.getMessage());
            }
            fromDateText.setText(startDate);
        }

        // Umwandlung in anzuzeigendes Enddatumsformat
        if (Character.toString(endDate.charAt(2)).equals("-")) {
            toDateText.setText(endDate);
        } else {
            try {
                Date date = dateFormat1.parse(endDate);
                endDate = dateFormat2.format(date);
            } catch (ParseException e) {
                Logger.log(this.getClass(), e.getMessage());
            }
            toDateText.setText(endDate);
        }
    }

    /**
     * Auswertung der Daten und Anzahl der Listenelemente
     * @param mContext
     * @return Rückgabe von "OK" oder Fehlermeldung
     */
    public String listGetTrips(Context mContext) {

        String result = "Unbekannter Fehler";

        // Zugriff auf verschlüsselten Anwendungsspeicher vorbereiten und Cookie auslesen
        CredentialStorageManager csm = new CredentialStorageManager(mContext);
        String sessionCookie = csm.getCookie();

        // Zeitraum berechnen
        Calendar cal = Calendar.getInstance();

        // Umwandel bei Änderung Startdatum und Enddatum vorbelegen beim Start
        if (Character.toString(startDate.charAt(2)).equals("-")) {
            try {
                Date date = dateFormat2.parse(startDate);
                startDate = dateFormat1.format(date);
            } catch (ParseException e) {
                Logger.log(this.getClass(), e.getMessage());
            }
        } else {
            endDate = dateFormat1.format(cal.getTime());
            cal.add(Calendar.DATE, -3); // Letzten 3 Tage
            startDate = dateFormat1.format(cal.getTime());
        }

        // Umwandlung von Enddatum
        if (Character.toString(endDate.charAt(2)).equals("-")) {
            try {
                Date date = dateFormat2.parse(endDate);
                endDate = dateFormat1.format(date);
            } catch (ParseException e) {
                Logger.log(this.getClass(), e.getMessage());
            }
        }


        // HttpClient initialisieren
        EkoioHttpClient smallTripsHttpClient = new EkoioHttpClient(EkoioHttpClient.SMALL_TRIPS_URL + "?first_datetime_0=" + startDate + "T00%3A00%3A00&first_datetime_1=" + endDate + "T23%3A59%3A59");
        EkoioHttpClient driveLogHttpClient = new EkoioHttpClient(EkoioHttpClient.DRIVE_LOGS_URL);

        // GET Request ausführen
        smallTripsHttpClient.makeRequest(EkoioHttpClient.GET, sessionCookie);
        String tripsJSON = smallTripsHttpClient.getResult();
        driveLogHttpClient.makeRequest(EkoioHttpClient.GET, sessionCookie);
        String driveLogsJSON = driveLogHttpClient.getResult();
        Logger.log(this.getClass(), "driveLogsDat: " + driveLogsJSON);

        // JSON-String in utils_trips-Objekt überführen und Informationen auslesen
        Gson gson   = new Gson();
        Trips trips = gson.fromJson(tripsJSON, Trips.class);

        // Kopie um Sachen nochmal auszulesen, wird unter anderem bei der Zeit verwendet, Daten können pro Element nur 1 mal ausgelesen werden
        Trips tripsCopy    = gson.fromJson(tripsJSON, Trips.class);



        // Anzahl der selbst angelegten Trips auslesen
        Pattern pattern = Pattern.compile("\"trip\":null");
        Matcher matcher = pattern.matcher(driveLogsJSON);
        newTripsCount = 0;
        while (matcher.find())
            newTripsCount++;
        Logger.log(this.getClass(), "Anzahl angelegter Fahrten " + newTripsCount);



        // auslesen der Ergebnismenge und Länge der Listenelemente festlegen
        rowCounter           = trips.getCount();
        dateEntries          = new String[rowCounter];
        distanceEntries      = new String[rowCounter];
        startAddressEntries  = new String[rowCounter];
        endAddressEntries    = new String[rowCounter];
        statusEntries        = new String[rowCounter];
        startTimeEntries     = new String[rowCounter];
        endTimeEntries       = new String[rowCounter];
        tripID               = new String[rowCounter];

        try {
            // Array mit Werten befüllen
            for (int i = 0; i < rowCounter; i++) {
                dateEntries[i]          = trips.getResults(i).getFormattedDate();
                distanceEntries[i]      = trips.getResults(i).getFormattedDistance();
                startAddressEntries[i]  = trips.getResults(i).getStartAddress().getFormattedCity();
                endAddressEntries[i]    = trips.getResults(i).getEndAddress().getFormattedCity();
                tripID[i]               = trips.getResults(i).getID();
                startTimeEntries[i]     = tripsCopy.getResults(i).getFormattedStartTime();
                endTimeEntries[i]       = tripsCopy.getResults(i).getFormattedEndTime();


                // Prüfung ob Fahrt kategorisiert ist
                if (trips.getResults(i).getDriveLog() == null) {
                    // Fahrt ist nicht kategorisert
                    tripStatus = "not categorized";
                } else {
                    // Fahrt ist kategorisiert --> Grüner Haken
                    tripStatus = "categorized";

                }
                statusEntries[i] = tripStatus;
            }
            result = "OK";
        } catch (ArrayIndexOutOfBoundsException e) {
            String maxCount = e.getMessage().split("index=")[1];
            result = String.format(getString(R.string.error_msg_max_trips), maxCount);
        }

        return result;
    }
}
