/*
 * EditorActivity
 * Fahrtenbuch
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2016.  | Tobias Donix, Kevin Neumann, Maximilian Staab
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.ekoio.drivelog.ui;

import android.app.Activity;
import android.content.Context;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ekoio.drivelog.R;
import com.ekoio.drivelog.utils.tripinfos.TripInfos;
import com.ekoio.drivelog.utils.tripinfos.TripsLogGet;
import com.ekoio.drivelog.utils.tripinfos.DriveLog;
import com.ekoio.drivelog.utils.EkoioWebportal;
import com.ekoio.drivelog.utils.Logger;
import com.ekoio.drivelog.utils.ToolbarOnMenuItemListener;
import com.ekoio.drivelog.utils.tripinfos.TripsAddress;

public class EditorActivity extends AppCompatActivity {

    public static final int EDITOR_REQUEST_CODE = 999;
    private TripInfos globalTripInfos;

    private AppCompatButton buttonSave, buttonCancel;
    private ProgressBar buttonSaveProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(0);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        this.buttonSaveProgress = (ProgressBar) findViewById(R.id.ac_edit_bt_save_progress);

        this.buttonCancel = (AppCompatButton) findViewById(R.id.ac_edit_bt_cancel);
        this.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }

        });
        //noinspection RestrictedApi
        this.buttonCancel.setSupportBackgroundTintList(getResources().getColorStateList(R.color.colorWhite));

        this.buttonSave = (AppCompatButton) findViewById(R.id.ac_edit_bt_save);
        this.buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isUpdateRequest = true;
                uploadDriveLog();
            }
        });
        //noinspection RestrictedApi
        this.buttonSave.setSupportBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));

        // Laden der Fahrtinformationen und ggf. Kategorie
        loadTripInfos();
    }

    /**
     * Ladeanimation ein/ausblenden und Speichern-Button deaktiverten
     * @param disableSaveButton true zum Deaktivieren des Speichern-Buttons
     * @param showProgressBar true zum Einblenden der Ladeanimation
     */
    private void showProgress(final boolean disableSaveButton, boolean showProgressBar) {
        String buttonText;
        int colorText;
        ColorStateList colorTint;
        if(disableSaveButton && !showProgressBar) {
            buttonText = getString(R.string.save);
            colorText = getResources().getColor(R.color.colorDarkGray);
            colorTint = getResources().getColorStateList(R.color.colorGray);
        } else if(disableSaveButton) {
            buttonText = "";
            colorText = getResources().getColor(R.color.colorDarkGray);
            colorTint = getResources().getColorStateList(R.color.colorGray);
        } else {
            buttonText = getString(R.string.save);
            colorText = getResources().getColor(R.color.colorWhite);
            colorTint = getResources().getColorStateList(R.color.colorPrimary);
        }
        //noinspection RestrictedApi
        this.buttonSave.setSupportBackgroundTintList(colorTint);
        this.buttonSave.setTextColor(colorText);
        this.buttonSave.setText(buttonText);
        this.buttonSave.setEnabled(!disableSaveButton);

        if(showProgressBar) {
            this.buttonSaveProgress.setVisibility(View.VISIBLE);
            this.buttonSaveProgress.getIndeterminateDrawable().setColorFilter(
                    getResources().getColor(R.color.colorDarkGray),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
        } else {
            this.buttonSaveProgress.setVisibility(View.GONE);
        }

    }

    /**
     * Asynchrones Laden der Kilometerstände
     */
    private void loadTripInfos() {
        class MileageDownloader extends AsyncTask<Object, Void, TripInfos> {

            Context context;
            String tripID;

            public MileageDownloader(Context context, String tripID) {
                this.context = context;
                this.tripID = tripID;
            }

            @Override
            protected void onPreExecute() {
                showProgress(true, true);
            }

            @Override
            protected TripInfos doInBackground(Object... params) {
                EkoioWebportal ekoioWebportal = new EkoioWebportal();
                TripInfos tripInfos = ekoioWebportal.getTripInfos(this.context, tripID);
                return tripInfos;
            }

            @Override
            protected void onPostExecute(TripInfos tripInfos) {

                if(tripInfos != null) {

                    /* Datum */
                    TextView tvDate = (TextView) findViewById(R.id.ac_edit_tv_date);
                    tvDate.setText(getDateString(tripInfos.getStartDateTime()));

                    /* Abfahrtszeit */
                    TextView tvStartDateTime = (TextView) findViewById(R.id.ac_edit_tv_time_start);
                    tvStartDateTime.setText(getTimeString(tripInfos.getStartDateTime()));
                    tvStartDateTime.setSelected(true);

                    /* Ankunftszeit */
                    TextView tvEndDateTime = (TextView) findViewById(R.id.ac_edit_tv_time_end);
                    tvEndDateTime.setText(getTimeString(tripInfos.getEndDateTime()));
                    tvEndDateTime.setSelected(true);

                    /* Kilometerstände */
                    String startMileageInKilometers = ((int) Math.round(tripInfos.getStartMileage() / 1000d)) + " " + getString(R.string.unit_km);
                    String endMileageInKilometers = ((int) Math.round(tripInfos.getEndMileage() / 1000d)) + " " + getString(R.string.unit_km);

                    TextView tvMileageStart = (TextView) findViewById(R.id.ac_edit_tv_mileage_start);
                    tvMileageStart.setText(startMileageInKilometers);
                    tvMileageStart.setSelected(true);

                    TextView tvMileageEnd = (TextView) findViewById(R.id.ac_edit_tv_mileage_end);
                    tvMileageEnd.setText(endMileageInKilometers);
                    tvMileageEnd.setSelected(true);

                    /* Startadresse */
                    TextView tvStartAdressStreet = (TextView) findViewById(R.id.ac_edit_tv_adress_street_start);
                    tvStartAdressStreet.setText(tripInfos.getStartStreet());
                    tvStartAdressStreet.setSelected(true);
                    TextView tvStartAdressCity = (TextView) findViewById(R.id.ac_edit_tv_adress_city_start);
                    tvStartAdressCity.setText(tripInfos.getStartCity());
                    tvStartAdressCity.setSelected(true);

                    /* Zieladresse */
                    TextView tvEndAdressStreet = (TextView) findViewById(R.id.ac_edit_tv_adress_street_end);
                    tvEndAdressStreet.setText(tripInfos.getEndStreet());
                    tvEndAdressStreet.setSelected(true);
                    TextView tvEndAdressCity = (TextView) findViewById(R.id.ac_edit_tv_adress_city_end);
                    tvEndAdressCity.setText(tripInfos.getEndCity());
                    tvEndAdressCity.setSelected(true);

                    /* Informationen der globalen TripInfos Variable zuweisen */
                    globalTripInfos = tripInfos;

                    String driveLogID = tripInfos.getDriveLogID();
                    if(driveLogID == null) {

                        /* Ladeanimation ausblenden */
                        showProgress(false, false);

                    } else {

                        /* Kategorie und Reisezweck/Notiz */
                        loadDriveLog(driveLogID);
                    }

                } else {
                    showProgress(true, false);
                    Toast.makeText(
                            this.context,
                            R.string.msg_failed_request_check_internet_connection,
                            Toast.LENGTH_SHORT).show();
                }

            }
        }

        /* Fahrtnummer */
        String tripID = getIntent().getStringExtra("trip");
        String headline = getString(R.string.activity_editor_title) + tripID;
        TextView textViewToolbar = (TextView) findViewById(R.id.comp_toolbar_textview_title);
        textViewToolbar.setText(headline);
        textViewToolbar.setTypeface(null, Typeface.BOLD);

        /* Restlichen Fahrtinformationen herunterladen */
        MileageDownloader mileageDownloader = new MileageDownloader(this, tripID);
        mileageDownloader.execute(this);
    }

    /**
     * Extrahieren der Uhrzeit aus Datum+Uhrzeit
     * @param utcDateTime YYYY-MM-DDTHH:MM:SSZ
     * @return HH::MM Uhr
     */

    public String getTimeString(String utcDateTime) {

        String timeString = getString(R.string.unknown);

        try {
        	DateFormat utc = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        	utc.setTimeZone(TimeZone.getTimeZone("UTC"));

        	Date date = utc.parse(utcDateTime);

        	DateFormat localeTime = new SimpleDateFormat("HH:mm");
        	localeTime.setTimeZone(TimeZone.getDefault());

        	timeString = localeTime.format(date) + " " + getString(R.string.unit_clock);

        } catch (ParseException e) {}

        return timeString;
    }

    /**
     * Extrahieren der Uhrzeit aus Datum+Uhrzeit
     * @param utcDateTime YYYY-MM-DDTHH:MM:SSZ
     * @return DD. {MM} YYYY
     */
    public String getDateString(String utcDateTime) {

        String timeString = getString(R.string.unknown);

        try {
            DateFormat utc = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            utc.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utc.parse(utcDateTime);

            DateFormat localeDate = new SimpleDateFormat("dd. MMMM yyyy");
            localeDate.setTimeZone(TimeZone.getDefault());

            timeString = localeDate.format(date);

        } catch (ParseException e) {}

        return timeString;
    }


    /**
     * Laden der Katgegorie, getankten Liter und des Reisezwecks/Notiz
     */
    private void loadDriveLog(String driveLogID) {
        class DriveLogDownloader extends AsyncTask<Object, Void, TripsLogGet> {

            Context context;
            String driveLogID;

            public DriveLogDownloader(Context context, String driveLogID) {
                this.context = context;
                this.driveLogID = driveLogID;
            }

            @Override
            protected void onPreExecute() {
                showProgress(true, true);
            }

            @Override
            protected TripsLogGet doInBackground(Object... params) {
                EkoioWebportal ekoioWebportal = new EkoioWebportal();
                TripsLogGet tripsLogGet = ekoioWebportal.getTripLog(this.context, this.driveLogID);
                return tripsLogGet;
            }

            @Override
            protected void onPostExecute(TripsLogGet tripLog) {
                if(tripLog != null) {

                    /* Kategorie */
                    RadioButton toggleBusiness = (RadioButton) findViewById(R.id.ac_edit_toggle_category_business);
                    RadioButton togglePrivate = (RadioButton) findViewById(R.id.ac_edit_toggle_category_private);
                    switch (tripLog.getType()) {
                        case 0:
                            toggleBusiness.setChecked(true);
                            togglePrivate.setChecked(false);
                            break;
                        case 1:
                            toggleBusiness.setChecked(false);
                            togglePrivate.setChecked(true);
                            break;
                        default:
                            toggleBusiness.setChecked(true);
                            togglePrivate.setChecked(false);
                            break;
                    }

                    /* Getankte Liter */
                    EditText etFuel = (EditText) findViewById(R.id.ac_edit_et_fuel);
                    etFuel.setText(tripLog.getRefuelledVolume());

                    /* Reisezweck / Notiz */
                    EditText etPurpose = (EditText) findViewById(R.id.ac_edit_et_purpose);
                    etPurpose.setText(tripLog.getTravelPurpose());

                    /* Ladeanimation ausblenden */
                    showProgress(false, false);

                } else {
                    showProgress(true, false);
                    Toast.makeText(
                            this.context,
                            R.string.msg_failed_request_trip_infos,
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
        DriveLogDownloader driveLogDownloader = new DriveLogDownloader(this, driveLogID);
        driveLogDownloader.execute(this);
    }


    /**
     * Hochladen des Fahrtbuchs mit Typ, getankten Liter und Reisezweck/Notiz
     */
    private void uploadDriveLog() {
        class DriveLogUpdater extends AsyncTask<Object, Void, String> {

            Context context;
            DriveLog driveLog;
            boolean isUpdateRequest;

            public DriveLogUpdater(Context context, DriveLog driveLog, boolean isUpdateRequest) {
                this.context = context;
                this.driveLog = driveLog;
                this.isUpdateRequest = isUpdateRequest;
            }

            @Override
            protected void onPreExecute() {
                showProgress(true, true);
            }

            @Override
            protected String doInBackground(Object... params) {
                EkoioWebportal ekoioWebportal = new EkoioWebportal();
                String returnMessage;
                if(isUpdateRequest)
                    returnMessage = ekoioWebportal.updateDriveLog(this.context, this.driveLog);
                else
                    returnMessage = ekoioWebportal.createDriveLog(this.context, this.driveLog);
                return returnMessage;
            }

            @Override
            protected void onPostExecute(String returnMessage) {
                if(returnMessage.equals(EkoioWebportal.DRIVE_LOG_SUCCESSFULL)) {
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                } else {
                    Toast.makeText(this.context, getString(R.string.error) + returnMessage, Toast.LENGTH_SHORT).show();
                    showProgress(false, false);
                }
            }
        }

        /* Kategorie */
        int type;
        RadioButton toggleBusiness = (RadioButton) findViewById(R.id.ac_edit_toggle_category_business);
        RadioButton togglePrivate = (RadioButton) findViewById(R.id.ac_edit_toggle_category_private);
        if(toggleBusiness.isChecked()) {
            type = 0;
        } else if (togglePrivate.isChecked()) {
            type = 1;
        } else {
            type = 0;
        }

        /* Getankte Liter */
        EditText etFuel = (EditText) findViewById(R.id.ac_edit_et_fuel);
        String refuelledVolume = etFuel.getText().toString();
        if(refuelledVolume.equals("")) refuelledVolume = "0.00";

        /* Reisezweck / Notiz */
        EditText etPurpose = (EditText) findViewById(R.id.ac_edit_et_purpose);
        String travelPurpose = etPurpose.getText().toString();
        if(travelPurpose.equals("")) travelPurpose = "";

        /* Start- und Zieladresse */
        TripsAddress startAddress = new TripsAddress(globalTripInfos.getStartCity(), globalTripInfos.getStartStreet());
        TripsAddress endAddress = new TripsAddress(globalTripInfos.getEndCity(), globalTripInfos.getEndStreet());

        /* Bestimmung, ob neuer Fahrtenbuch-Eintrag oder Aktualisierung */
        boolean isUpdateRequest = false;
        Integer driveLogID = null;
        if(globalTripInfos.getDriveLogID() != null) {
            isUpdateRequest = true;
            driveLogID = Integer.parseInt(globalTripInfos.getDriveLogID());
        }

        DriveLog driveLog = new DriveLog(
                type,
                globalTripInfos.getStartDateTime(),
                globalTripInfos.getEndDateTime(),
                globalTripInfos.getDistance(),
                startAddress,
                endAddress,
                globalTripInfos.getTripID(),
                travelPurpose,
                refuelledVolume,
                globalTripInfos.getStartMileage(),
                globalTripInfos.getEndMileage(),
                driveLogID
        );

        DriveLogUpdater driveLogUpdater = new DriveLogUpdater(this, driveLog, isUpdateRequest);
        driveLogUpdater.execute(this);
    }
}
